<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;

class CompanyExport implements FromView, WithEvents
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.company.export', ['data' => $this->data]);
    }

    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
            for ($column = 'A'; $column <= 'R'; $column++) { $event->sheet->getDelegate()->getColumnDimension($column)->setAutoSize(true);}
        }];
    }
}
