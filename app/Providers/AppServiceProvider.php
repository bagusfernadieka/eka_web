<?php

namespace App\Providers;

use App\Models\Umum;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($e){
            $umum = Umum::first();

            $e->with([
                'tanggal_sekarang' => Carbon::now()->isoFormat('dddd, DD MMMM YYYY'),
                'title_web' => $umum->nama,
                'logo' => ($umum->logo === NULL || $umum->logo === '' || $umum->logo == 'logo') ? env('APP_NAME') : '<img src="'.url('/storage/foto/'.$umum->logo).'" height="50">',
                'logo_login' => ($umum->logo === NULL || $umum->logo === '' || $umum->logo == 'logo') ? env('APP_NAME') : '<img src="'.url('/storage/foto/'.$umum->logo).'" height="60">',
                'logo_landing' => ($umum->logo === NULL || $umum->logo === '' || $umum->logo == 'logo') ? env('APP_NAME') : '<img src="'.url('/storage/foto/'.$umum->logo).'" height="40">',
                'favicon' => ($umum->favicon === NULL || $umum->favicon === '' || $umum->favicon == 'favicon') ? env('APP_NAME') : ''.url('/storage/foto/'.$umum->favicon).'',
            ]);
        });
    }
}
