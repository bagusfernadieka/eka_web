<?php

namespace App\Http\Controllers;

use App\Facade\Weblog;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;
use Laratrust\LaratrustFacade as Laratrust;
use Maatwebsite\Excel\Facades\Excel;
use Proengsoft\JsValidation\Facades\JsValidatorFacade as JSvalidation;
use Intervention\Image\Facades\Image;
use App\Models\User;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:product-read')->only('index');
        $this->middleware('permission:product-create')->only(['create', 'store']);
        $this->middleware('permission:product-update')->only(['edit', 'update']);
        $this->middleware('permission:product-delete')->only('delete');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.product.index');
    }

    public function ajax(Request $request)
    {
        // filter
        $cari = $request->cari;
        
        // set query
        $data = Product::query()
            ->when($cari, fn ($e, $cari) => $e->where(function ($e) use ($cari) {
                $e->where('name', 'like', '%' . $cari . '%')->orWhere('price', 'like', '%' . $cari . '%');
            }))
            ->where('status', $request->status)
            ->orderBy('id','asc');

        // dd($data->get()->toArray());

        // create datatable
        return DataTables::eloquent($data)
            ->addColumn('photo_mod', function ($e) {
                $photo = ($e->photo === "" || $e->photo === null) ? '/storage/product/photo.png' : '/storage/product/' . $e->photo;
                return '<a href="' . url($photo) . '" data-title="' . $e->name . '" style="height:50px; overflow:hidden; border-radius:4px;" class="detail-foto"><img src="' . url($photo) . '" class="rounded" width="40"/></a>';
            })
            ->addColumn('time_input_mod', function ($e) {
                // Menggunakan Carbon untuk mengubah format tanggal
                $formattedDate = Carbon::parse($e->created_at)->isoFormat('DD MMMM YYYY');
                
                return $formattedDate;
            })
            ->addColumn('price_mod', function ($e) {
                $data = '
                    <div class="d-flex align-items-center">
                        <b class="text-muted">' . 'Rp. ' . number_format($e->price, 0, ',', '.') . '</b>
                    </div>
                ';
                return $data;
            })
            ->addColumn('aksi', function ($e) {
                $btnEdit = Laratrust::isAbleTo('product-update') ? '<a href="' . route('product.edit', ['product' => $e->id]) . '" class="btn btn-xs "><i class="bx bx-edit"></i></a>' : '';
                $btnDelete = Laratrust::isAbleTo('product-delete') ?  '<a href="' . route('product.destroy', ['product' => $e->id]) . '" data-title="' . $e->name . '" class="btn btn-xs text-danger btn-hapus"><i class="bx bx-trash"></i></a>' : '';
                $btnReload = Laratrust::isAbleTo('product-update') ? '<a href="' . route('product.destroy', ['product' => $e->id]) . '" data-title="' . $e->name . '" data-status="' . $e->status . '" class="btn btn-outline-secondary btn-xs btn-hapus"><i class="bx bx-refresh"></i></i></a>' : '';

                if ($e->status == 'y') {
                    return $btnEdit . ' ' . $btnDelete;
                } else {
                    return $btnReload;
                }
            })
            ->rawColumns(['photo_mod','time_input_mod', 'price_mod', 'aksi'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // validate form
        $validator = JSvalidation::make([
            'name'  => 'required|min:3',
            'price'  => 'required|min:0',
            'foto' => 'nullable',
        ]);

        // page
        return view('backend.product.create', compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate form
        $request->validate([
            'name'  => 'required|min:3',
            'price'  => 'required|min:0',
            'foto' => 'nullable',
        ]);

        // collect data
        $record = [
            'name' => $request->name,
            'price' => $request->price,
            'photo' => $request->foto,
        ];

        DB::beginTransaction();
        try {
            // insert data
            Product::create($record);
            DB::commit();

            // create log
            Weblog::set('Create Product : ' . $request->name);

            // redirect index page
            return redirect(route('product.index'))->with(['pesan' => '<div class="alert alert-success">Data successfully created</div>']);
        } catch (\Throwable $th) {
            // error handle
            DB::rollBack();
            Log::info($th->getMessage());
            return redirect()->back()->with(['pesan' => '<div class="alert alert-danger">An error occurred, please try again</div>']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        // validate form
        $validator = JSvalidation::make([
            'name'  => 'required|min:3',
            'price'  => 'required|min:0',
            'foto' => 'nullable',
        ]);

        // page
        return view('backend.product.edit', compact('validator', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        // validate form
        $validasi = $request->validate([
            'name'  => 'required|min:3',
            'price'  => 'required|min:0',
            'foto' => 'nullable',
        ]);

        DB::beginTransaction();
        try {
            // update data
            Product::find($product->id)->update([
                'name' => $request->name,
                'price' => $request->price,
                'photo' => $request->foto,
            ]);
            DB::commit();

            // create log
            Weblog::set('Update product name ' . $product->name . ' to ' . $request->name);

            // redirect index
            return redirect(route('product.index'))->with(['pesan' => '<div class="alert alert-success">Data successfully updated</div>']);
        } catch (\Throwable $th) {
            // error handle
            DB::rollBack();
            Log::warning($th->getMessage());
            return redirect()->back()->with(['pesan' => '<div class="alert alert-danger">An error occurred, please try again</div>']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        DB::beginTransaction();
        try {
            // delete
            $status = Product::find($product->id)->status;
            if ($status == 'y') {
                Product::find($product->id)->update([
                    'status' => 'n'
                ]);
                DB::commit();
                Weblog::set('Disable product : ' . $product->name);

                return response()->json([
                    'tipe' => true,
                    'pesan' => 'Data successfully deleted'
                ]);
            } else {
                // activate
                Product::find($product->id)->update([
                    'status' => 'y'
                ]);
                DB::commit();
                Weblog::set('Activate product : ' . $product->name);

                return response()->json([
                    'tipe' => true,
                    'pesan' => 'Data successfully reactivated'
                ]);
            }
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'tipe' => false,
                'pesan' => $e->errorInfo,
            ]);
        }
    }

    public function ganti_foto(Request $request)
    {
        if ($request->has('file')) {
            $file = $request->file;
            $request->validate([
                'file' => 'required|image|max:2000'
            ]);

            $name = time();
            $ext  = $file->getClientOriginalExtension();
            $foto = $name . '.' . $ext;

            $path = $file->getRealPath();
            $thum = Image::make($path)->resize(80, 80, function ($size) {
                $size->aspectRatio();
            });
            $thumPath = public_path('/storage/product') . '/thum_' . $foto;
            $thum = Image::make($thum)->save($thumPath);

            $request->file->storeAs('public/product', $foto);

            return response()->json([
                'file' => $foto,
            ]);
        }
    }

    public function simpan_foto(Request $request)
    {
        DB::beginTransaction();
        try {
            User::find(Auth::id())->update(['foto' => $request->foto]);
            DB::commit();

            Weblog::set('Update Profile Photo');

            return response()->json([
                'pesan' => 'Photo successfully updated'
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response([
                'pesan' => 'An error occurred'
            ]);
        }
    }
}
