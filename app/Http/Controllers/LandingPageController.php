<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Banner;
use App\Models\Product;
use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    public function index()
    {
        $about = About::first();
        $banner = Banner::where('status', 'y')->get();
        $product = Product::where('status', 'y')->skip(0)->take(6)->get();
        return view('backend.landingpage.index', compact('about','banner', 'product'));
    }
}
