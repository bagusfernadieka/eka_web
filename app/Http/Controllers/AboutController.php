<?php

namespace App\Http\Controllers;

use App\Facade\Weblog;
use App\Models\About;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Proengsoft\JsValidation\Facades\JsValidatorFacade as JSvalidation;

class AboutController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:about-read')->only('index');
        $this->middleware('permission:about-create')->only(['create', 'store']);
        $this->middleware('permission:about-update')->only(['edit', 'update']);
        $this->middleware('permission:about-delete')->only('delete');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // validate form
        $validator = JSvalidation::make([
            'about'  => 'required|min:3',
            'phone'  => 'required|min:3',
            'email' => 'email|required|min:3',
            'address'  => 'required|min:3',
        ]);

        $about = About::first();
        return view('backend.about.edit', compact('about', 'validator'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, About $about)
    {
        // validate form
        $validasi = $request->validate([
            'about'  => 'required|min:3',
            'phone'  => 'required|min:3',
            'email' => 'email|required|min:3',
            'address'  => 'required|min:3',
        ]);

        DB::beginTransaction();
        try {
            // update data
            About::find($about->id)->update([
                'about' => $request->about,
                'phone' => $request->phone,
                'email' => $request->email,
                'address' => $request->address,
                'user_update' => Auth::id(),
            ]);
            DB::commit();

            // create log
            Weblog::set('Update about');

            // redirect index
            return redirect(route('about.index'))->with(['pesan' => '<div class="alert alert-success">Data successfully updated</div>']);
        } catch (\Throwable $th) {
            // error handle
            DB::rollBack();
            Log::warning($th->getMessage());
            return redirect()->back()->with(['pesan' => '<div class="alert alert-danger">An error occurred, please try again</div>']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
