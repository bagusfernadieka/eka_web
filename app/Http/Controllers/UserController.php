<?php

namespace App\Http\Controllers;

use App\Exports\UserExport;
use App\Facade\Weblog;
use App\Http\Requests\PasswordRequest;
use App\Models\Company;
use App\Models\RoleUser;
use App\Models\User;
use App\Models\UserCompany;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\Facades\DataTables;
use Laratrust\LaratrustFacade as Laratrust;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:user-read')->only('index');
        $this->middleware('permission:user-create')->only(['create', 'store']);
        $this->middleware('permission:user-update')->only(['edit', 'update']);
        $this->middleware('permission:user-delete')->only('delete');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('backend.users.index');
    }

    public function ajax(Request $request)
    {
        $status = $request->status;
        $cari = $request->cari;

        $data = User::query()
            ->with('role_user')
            ->with('user_company_orm', fn ($e) => $e->where('status', 'y'))
            ->when($cari, function ($e, $cari) {
                $e->where(function ($e) use ($cari) {
                    return $e->where('username', 'like', '%' . $cari . '%')->orWhere('nama', 'like', '%' . $cari . '%')->orWhere('email', 'like', '%' . $cari . '%');
                });
            })
            ->where('id', '<>', 1)
            ->where('status', $status)
            ->orderBy('id', 'desc')
            ->get();

        if ($request->filled('export')) {
            return Excel::download(new UserExport($data, $request->all()), 'USER.xlsx');
        }

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('foto_mod', function ($e) {
                $foto = ($e->foto === "" || $e->foto === null) ? '/backend/sneat-1.0.0/assets/img/avatars/1.png' : '/storage/foto/' . $e->foto;
                return '<a href="' . url($foto) . '" data-title="' . $e->nama . '" style="height:50px; overflow:hidden; border-radius:4px;" class="detail-foto"><img src="' . url($foto) . '" class="rounded" width="40"/></a>';
            })
            ->editColumn('created_at', function ($e) {
                return '<div class="badge bg-dark rounded-pill">' . Carbon::parse($e->created_at)->isoFormat('DD MMM YYYY HH:mm') . '</div>';
            })
            ->addColumn('role', function ($e) {
                $roles = [];
                foreach ($e->role_user as $item) {
                    $roles[] = '<span class="badge bg-dark">' . $item->name . '</span>';
                }

                return implode(' ', $roles);
            })
            ->addColumn('company_mod', function ($e) {
                $userCompanyMod = [];
                foreach ($e->user_company_orm as $item) {
                    $userCompanyMod[] = '<span class="badge bg-secondary">' . $item->company_code . ' - ' . $item->company_name . '</span>';
                }

                if( count($userCompanyMod) > 0 ) {
                    return implode(' ', $userCompanyMod);
                } else {
                    return '<span class="badge bg-secondary">ALL COMPANY</span>';
                }
            })
            ->addColumn('aksi', function ($e) {
                $btnEdit = Laratrust::isAbleTo('user-update') ? '<a href="' . route('user.edit', ['user' => $e->id]) . '" class="btn btn-xs "><i class="bx bx-edit"></i></a>' : '';
                $btnDelete = Laratrust::isAbleTo('user-delete') ?  '<a href="' . route('user.destroy', ['user' => $e->id]) . '" data-title="' . $e->username . '" class="btn btn-xs text-danger btn-hapus"><i class="bx bx-trash"></i></a>' : '';
                $btnReload = Laratrust::isAbleTo('user-update') ? '<a href="' . route('user.destroy', ['user' => $e->id]) . '" data-title="' . $e->username . '" data-status="' . $e->status . '" class="btn btn-outline-secondary btn-xs btn-hapus"><i class="bx bx-refresh"></i></i></a>' : '';
                $btnPassword = Laratrust::isAbleTo('user-update') ? '<button type="button"
                                                                                    class="btn btn-xs btn-open-ganti-password"
                                                                                    data-id="' . $e->id . '"
                                                                                    data-username="' . $e->username . '"
                                                                                    data-email="' . $e->email . '"
                                                                                    data-nama="' . $e->nama . '"
                                                                                    ><i class="bx bxs-key"></i></button>' : '';
                $btnForceLogin = Laratrust::isAbleTo('user-update') ? '<a href="' . route('force-login', ['id' => $e->id]) . '" class="btn btn-xs"><i class="bx bxs-arrow-from-left" title="Force login"></i></a>' : '';

                if ($e->status == 'y') {
                    return $btnForceLogin . ' ' . $btnPassword . ' ' . $btnEdit . ' ' . $btnDelete;
                } else {
                    return $btnReload;
                }
            })
            ->rawColumns(['foto_mod', 'aksi', 'created_at', 'role', 'company_mod'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validator = JsValidatorFacade::make([
            'username' => 'required|unique:users|alpha_dash|min:3',
            'nama' => 'required|min:3',
            'telephone' => 'required|unique:users,user_telpon|min:3',
            'email' => 'email|required|min:3',
            'foto' => 'nullable',
            'role' => 'required'
        ]);

        return view('backend.users.create', compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validasi = $request->validate([
            'username' => 'required|unique:users|alpha_dash|min:3',
            'nama' => 'required|min:3',
            'telephone' => 'required|unique:users,user_telpon|min:3',
            'email' => 'email|required|min:3',
            'foto' => 'nullable',
            'role' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $user = User::create([
                'nama' => $request->nama,
                'username' => $request->username,
                'user_telpon' => $request->telephone,
                'email' => $request->email,
                'foto' => $request->foto,
                'password' => Hash::make('password')
            ]);

            // input role
            $user->attachRoles($request->role);

            // input company
            $this->__userCompany($request, $user);

            DB::commit();
            Weblog::set('Create user ' . $request->username);

            return redirect(route('user.index'))->with([
                'pesan' => '<div class="alert alert-success">User ' . $request->username . ' successfully created</div>',
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th->getMessage());
            return redirect()->back()->with([
                // 'pesan' => '<div class="alert alert-danger">An error occurred, please try again</div>',
                'pesan' => $th->getMessage(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        // validate
        $validator = JsValidatorFacade::make([
            'username' => 'required|alpha_dash|min:3|unique:users,username,' . $user->id,
            'nama' => 'required|min:3',
            'telephone' => 'required|min:3|unique:users,user_telpon,' . $user->id,
            'email' => 'required|min:3|email|unique:users,email,' . $user->id,
            'foto' => 'nullable',
            'role' => 'required'
        ]);

        // get user_company_id from orm belongsToMany
        $company = count($user->user_company_orm) > 0 ? $user->user_company_orm->where('status', 'y') : null;

        // dd($company);
        return view('backend.users.edit', compact('validator', 'user', 'company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validasi = $request->validate([
            'username' => 'required|min:3|alpha_dash|unique:users,username,' . $user->id,
            'nama' => 'required|min:3',
            'telephone' => 'required|min:3|unique:users,user_telpon,' . $user->id,
            'email' => 'required|min:3|email|unique:users,email,' . $user->id,
            'foto' => 'nullable',
            'leader' => 'nullable'
        ]);

        DB::beginTransaction();
        try {
            User::find($user->id)->update([
                'nama' => $request->nama,
                'username' => $request->username,
                'user_telpon' => $request->telephone,
                'email' => $request->email,
                'foto' => $request->foto,
                'password' => Hash::make('password')
            ]);
            
            // delete and add new role
            RoleUser::where('user_id', $user->id)->delete();
            $user->attachRoles($request->role);

            // delete and add new company
            UserCompany::where('user_id', $user->id)->delete();
            $this->__userCompany($request, $user);

            DB::commit();

            Weblog::set('Update profile : ' . $request->username);
            return redirect(route('user.index'))->with([
                'pesan' => '<div class="alert alert-success">Profile successfully updated</div>'
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th->getMessage());
            return redirect(route('user.index'))->with([
                'pesan' => '<div class="alert alert-danger">An error occurred, please try again</div>'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $status = $user->status;
        DB::beginTransaction();
        try {
            if ($status == 'y') {
                User::find($user->id)->update(['status' => 'n']);
                Weblog::set('Delete user : ' . $user->username);
            } else {
                User::find($user->id)->update(['status' => 'y']);
                Weblog::set('Activate user : ' . $user->username);
            }

            DB::commit();

            return response()->json([
                'pesan' => 'Data successfully deleted',
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            Weblog::set($th->getMessage());
            return response()->json([
                'pesan' => 'An error occurred'
            ], 500);
        }
    }

    public function ganti_password(PasswordRequest $request)
    {
        DB::beginTransaction();
        try {
            User::find($request->id)->update(['password' => Hash::make($request->password)]);
            DB::commit();
            Weblog::set('Change password username : ' . $request->username);
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th->getMessage());
            return response()->json([
                'errors' => 'An error occurred, please try again'
            ], 500);
        }
        return response()->json([
            'message' => 'success'
        ]);
    }

    public function profil(Request $request)
    {
        $validasi = [
            'nama' => 'required',
            'email' => 'required|email|unique:users,email,' . Auth::id(),
        ];

        if ($request->method() === 'POST') {
            $request->validate($validasi);

            DB::beginTransaction();
            try {
                User::where('id', Auth::id())->update(['nama' => $request->nama, 'email' => $request->email]);
                DB::commit();

                Weblog::set('Update profile');
                return redirect(route('profil'))->with([
                    'pesan' => '<div class="alert alert-success">Profile successfully updated</div>'
                ]);
            } catch (\Throwable $th) {
                DB::rollBack();
                return redirect(route('profil'))->with([
                    'pesan' => '<div class="alert alert-danger">' . $th->getMessage() . '</div>'
                ]);
            }
        }

        $validator = JsValidatorFacade::make($validasi);

        $user = User::find(Auth::id());
        return view('backend.pengaturan.show', compact('user', 'validator'));
        return view('backend.users.edit', compact('validator', 'user'));
    }

    public function password(Request $request)
    {
        if ($request->method() === 'POST') {
            if (Hash::check($request->old_password, Auth::user()->password)) {

                DB::beginTransaction();
                try {
                    User::find(Auth::id())->update(['password' => Hash::make($request->password)]);
                    DB::commit();

                    Weblog::set('Change password');
                    return redirect(route('password'))->with([
                        'pesan' => '<div class="alert alert-success mt-2">Password successfully updated</div>'
                    ]);
                } catch (\Throwable $e) {
                    DB::rollBack();

                    Weblog::set('Change password but failed');
                    return redirect(route('password'))->with([
                        'pesan' => '<div class="alert alert-danger">An error occurred, please try again</div>'
                    ]);
                }
            } else {
                return redirect(route('password'))->with([
                    'pesan' => '<div class="alert alert-danger mt-2">Your old password is incorrect!</div>'
                ]);
            }
        }

        $validator = JsValidatorFacade::make([
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed'
        ]);

        return view('backend.pengaturan.ganti_password', compact('validator'));
    }

    public function weblog(Request $request)
    {
        if ($request->method() === 'POST') {
        }

        return view('backend.pengaturan.ganti_password');
    }

    public function ganti_foto(Request $request)
    {
        if ($request->has('file')) {
            $file = $request->file;
            $request->validate([
                'file' => 'required|image|max:2000'
            ]);

            $name = time();
            $ext  = $file->getClientOriginalExtension();
            $foto = $name . '.' . $ext;

            $path = $file->getRealPath();
            $thum = Image::make($path)->resize(80, 80, function ($size) {
                $size->aspectRatio();
            });
            $thumPath = public_path('/storage/foto') . '/thum_' . $foto;
            $thum = Image::make($thum)->save($thumPath);

            $request->file->storeAs('public/foto', $foto);

            return response()->json([
                'file' => $foto,
            ]);
        }
    }

    public function simpan_foto(Request $request)
    {
        DB::beginTransaction();
        try {
            User::find(Auth::id())->update(['foto' => $request->foto]);
            DB::commit();

            Weblog::set('Update Profile Photo');

            return response()->json([
                'pesan' => 'Photo successfully updated'
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response([
                'pesan' => 'An error occurred'
            ]);
        }
    }

    private function __userCompany($request, $user)
    {
        // if choose company
        if ($request->exists('company')) {
            $company = Company::query()
                ->whereIn('id', $request->company)
                ->get()->pluck('id')->toArray();

            $userCompany = collect($company)->map(function ($item) use ($user) {
                return [
                    'user_id' => $user->id,
                    'company_id' => $item,
                    'user_input' => Auth::id(),
                ];
            })->toArray();
            // dd($userCompany);
    
            UserCompany::insert($userCompany);
        } 
    }
}
