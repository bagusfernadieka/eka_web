<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Redirect;
use App\Exports\CompanyExport;
use App\Models\Company;
use App\Models\Product;
use Laratrust\LaratrustFacade as Laratrust;

class HomeController extends Controller
{
    public function index()
    {
        // Redirect to the 'example.route' route
        return view('backend.dashboard');
    }

    public function ajax(Request $request)
    {
        // filter
        $cari = $request->cari;
        
        // set query
        $data = Product::query()
            ->when($cari, fn ($e, $cari) => $e->where(function ($e) use ($cari) {
                $e->where('name', 'like', '%' . $cari . '%')->orWhere('price', 'like', '%' . $cari . '%');
            }))
            ->where('status', 'y')
            ->orderBy('id','asc');

        // dd($data->get()->toArray());

        // create datatable
        return DataTables::eloquent($data)
            ->addColumn('photo_mod', function ($e) {
                $photo = ($e->photo === "" || $e->photo === null) ? '/storage/product/photo.png' : '/storage/product/' . $e->photo;
                return '<a href="' . url($photo) . '" data-title="' . $e->name . '" style="height:50px; overflow:hidden; border-radius:4px;" class="detail-foto"><img src="' . url($photo) . '" class="rounded" width="40"/></a>';
            })
            ->addColumn('time_input_mod', function ($e) {
                // Menggunakan Carbon untuk mengubah format tanggal
                $formattedDate = Carbon::parse($e->created_at)->isoFormat('DD MMMM YYYY');
                
                return $formattedDate;
            })
            ->addColumn('price_mod', function ($e) {
                $data = '
                    <div class="d-flex align-items-center">
                        <b class="text-muted">' . 'Rp. ' . number_format($e->price, 0, ',', '.') . '</b>
                    </div>
                ';
                return $data;
            })
            ->rawColumns(['photo_mod','time_input_mod', 'price_mod'])
            ->make(true);
    }
}
