<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Exam;
use App\Models\ExamType;
use App\Models\Group;
use App\Models\Role;
use App\Models\Room;
use App\Models\UserCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    public function role(Request $request)
    {
        $term = $request->term;
        $data = Role::query()
            ->when($term, function ($e, $term) {
                $e->where('name', 'like', '%' . $term . '%');
            })
            ->select('id', 'name as label');

        if ($data->count() > 0) {
            return response()->json([
                'data'  => $data->get(),
                'status' => true
            ]);
        } else {
            return response()->json([
                'status' => false,
                'data'  => null,
            ]);
        }
    }

    public function company(Request $request)
    {
        $term = $request->term;

        // get company_arr from user_id login
        $company_arr = UserCompany::where('user_id', Auth::id())->get()->pluck('company_id')->toArray();

        $data = Company::query()
            ->where('status', 'y')
            ->when($term, function ($e, $term) {
                $e->where('company_code', 'like', '%' . $term . '%')->orWhere('company_name', 'like', '%' . $term . '%');
            })
            ->select('id as id', DB::raw('concat(company_code, " - ", company_name) as label'))
            ->orderBy('company_name', 'asc');

        // if user login have company_id
        if( count($company_arr) > 0 ) {
            $data->whereIn('id', $company_arr);
        }

        if ($data->count() > 0) {
            return response()->json([
                'data'  => $data->get(),
                'status' => true
            ]);
        } else {
            return response()->json([
                'status' => false,
                'data'  => null,
            ]);
        }
    }

    public function group(Request $request)
    {
        $term = $request->term;

        $data = Group::query()
            ->where('status', 'y')
            ->when($term, function ($e, $term) {
                $e->where('group_code', 'like', '%' . $term . '%')->orWhere('group_name', 'like', '%' . $term . '%');
            })
            ->select('id as id', DB::raw('concat(group_code, " - ", group_name) as label'))
            ->orderBy('group_name', 'asc');

        if ($data->count() > 0) {
            return response()->json([
                'data'  => $data->get(),
                'status' => true
            ]);
        } else {
            return response()->json([
                'status' => false,
                'data'  => null,
            ]);
        }
    }

    public function examtype(Request $request)
    {
        $term = $request->term;

        $data = ExamType::query()
            ->where('status', 'y')
            ->when($term, function ($e, $term) {
                $e->where('exam_type_code', 'like', '%' . $term . '%')->orWhere('exam_type_name', 'like', '%' . $term . '%');
            })
            ->select('id as id', DB::raw('concat(exam_type_code, " - ", exam_type_name) as label'))
            ->orderBy('exam_type_name', 'asc');

        if ($data->count() > 0) {
            return response()->json([
                'data'  => $data->get(),
                'status' => true
            ]);
        } else {
            return response()->json([
                'status' => false,
                'data'  => null,
            ]);
        }
    }

    public function method(Request $request)
    {
        $term = $request->term;

        // statis without db
        $staticData = [
            ['id' => 'written', 'label' => 'Written'],
            ['id' => 'practical', 'label' => 'Practical'],
        ];

        // Filter based term
        $filteredData = collect($staticData)
            ->when($term, function ($collection) use ($term) {
                return $collection->filter(function ($item) use ($term) {
                    return stripos($item['label'], $term) !== false;
                });
            })
            ->sortBy('label')
            ->values(); // Re-index array

        if ($filteredData->count() > 0) {
            return response()->json([
                'data'  => $filteredData->toArray(),
                'status' => true
            ]);
        } else {
            return response()->json([
                'status' => false,
                'data'  => null,
            ]);
        }
    }

    public function grade(Request $request)
    {
        $term = $request->term;

        // statis without db
        $staticData = [
            ['id' => 1, 'label' => 1],
            ['id' => 2, 'label' => 2],
            ['id' => 3, 'label' => 3],
            ['id' => 4, 'label' => 4],
            ['id' => 5, 'label' => 5],
            ['id' => 6, 'label' => 6],
            ['id' => 7, 'label' => 7],
            ['id' => 8, 'label' => 8],
            ['id' => 9, 'label' => 9],
            ['id' => 10, 'label' => 10],
            ['id' => 11, 'label' => 11],
            ['id' => 12, 'label' => 12],
        ];

        // Filter based term
        $filteredData = collect($staticData)
            ->when($term, function ($collection) use ($term) {
                return $collection->filter(function ($item) use ($term) {
                    return stripos($item['label'], $term) !== false;
                });
            })
            ->sortBy('label')
            ->values(); // Re-index array

        if ($filteredData->count() > 0) {
            return response()->json([
                'data'  => $filteredData->toArray(),
                'status' => true
            ]);
        } else {
            return response()->json([
                'status' => false,
                'data'  => null,
            ]);
        }
    }

    public function fase(Request $request)
    {
        $term = $request->term;

        // statis without db
        $staticData = [
            ['id' => 1, 'label' => 1],
            ['id' => 2, 'label' => 2],
            ['id' => 3, 'label' => 3],
        ];

        // Filter based term
        $filteredData = collect($staticData)
            ->when($term, function ($collection) use ($term) {
                return $collection->filter(function ($item) use ($term) {
                    return stripos($item['label'], $term) !== false;
                });
            })
            ->sortBy('label')
            ->values(); // Re-index array

        if ($filteredData->count() > 0) {
            return response()->json([
                'data'  => $filteredData->toArray(),
                'status' => true
            ]);
        } else {
            return response()->json([
                'status' => false,
                'data'  => null,
            ]);
        }
    }

    public function exam(Request $request)
    {
        $term = $request->term;
        $company = $this->convertArray($request->company);

        $data = Exam::query()
            ->where('status', 'y')
            ->when($term, function ($e, $term) {
                $e->where('exam_code', 'like', '%' . $term . '%')->orWhere('exam_name', 'like', '%' . $term . '%');
            })
            ->when($company, function ($e, $company) {
                $e->whereIn('company_id', $company);
            })
            ->select('id as id', DB::raw('concat(exam_code, " - ", exam_name) as label'))
            ->orderBy('exam_name', 'asc');

        if ($data->count() > 0) {
            return response()->json([
                'data'  => $data->get(),
                'status' => true
            ]);
        } else {
            return response()->json([
                'status' => false,
                'data'  => null,
            ]);
        }
    }

    public function room(Request $request)
    {
        $term = $request->term;
        $company = $this->convertArray($request->company);

        $data = Room::query()
            ->where('status', 'y')
            ->when($term, function ($e, $term) {
                $e->where('room_code', 'like', '%' . $term . '%')->orWhere('room_name', 'like', '%' . $term . '%');
            })
            ->when($company, function ($e, $company) {
                $e->whereIn('company_id', $company);
            })
            ->select('id as id', DB::raw('concat(room_code, " - ", room_name) as label'))
            ->orderBy('room_name', 'asc');

        if ($data->count() > 0) {
            return response()->json([
                'data'  => $data->get(),
                'status' => true
            ]);
        } else {
            return response()->json([
                'status' => false,
                'data'  => null,
            ]);
        }
    }

    public function surveillancetype(Request $request)
    {
        $term = $request->term;

        // statis without db
        $staticData = [
            ['id' => 'effective', 'label' => 'Effective'],
            ['id' => 'substitute', 'label' => 'Substitute'],
            ['id' => 'corrector', 'label' => 'Corrector'],
        ];

        // Filter based term
        $filteredData = collect($staticData)
            ->when($term, function ($collection) use ($term) {
                return $collection->filter(function ($item) use ($term) {
                    return stripos($item['label'], $term) !== false;
                });
            })
            ->sortBy('label')
            ->values(); // Re-index array

        if ($filteredData->count() > 0) {
            return response()->json([
                'data'  => $filteredData->toArray(),
                'status' => true
            ]);
        } else {
            return response()->json([
                'status' => false,
                'data'  => null,
            ]);
        }
    }

    public function convertArray($data)
    {
        if ($data) {
            if (gettype($data) === 'string') {
                $data_arr = [];
                array_push($data_arr, $data);
            } else {
                $data_arr = $data;
            }
        } else {
            $data_arr = null;
        }


        return $data_arr;
    }
}
