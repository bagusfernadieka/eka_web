<?php

namespace App\Http\Controllers;

use App\Facade\Weblog;
use App\Models\Umum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;

class UmumController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:umum-read')->only('index');
        $this->middleware('permission:umum-create')->only(['create', 'store']);
        $this->middleware('permission:umum-update')->only(['edit', 'update']);
        $this->middleware('permission:umum-delete')->only('delete');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengaturan = Umum::first();

        return view('backend.umum.show', compact('pengaturan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Umum  $umum
     * @return \Illuminate\Http\Response
     */
    public function show(Umum $umum)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Umum  $umum
     * @return \Illuminate\Http\Response
     */
    public function edit(Umum $umum)
    {
        $validator = JsValidatorFacade::make([
            'nama' => 'required',
            'foto' => 'nullable',
            'favicon' => 'nullable'
        ]);

        return view('backend.umum.edit', compact('umum', 'validator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Umum  $umum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Umum $umum)
    {
        $validasi = $request->validate([
            'nama' => 'required',
            'logo' => 'nullable',
            'favicon' => 'nullable'
        ]);

        DB::beginTransaction();
        try {
            if ($request->logo <> '') {
                Umum::find($umum->id)->update(['logo' => $request->logo]);
            }

            if ($request->favicon <> '') {
                Umum::find($umum->id)->update(['favicon' => $request->favicon]);
            }

            Umum::find($umum->id)->update(['nama' => $request->nama]);

            DB::commit();
            Weblog::set('Change website setting');

            return redirect(route('umum.index'))->with([
                'pesan' => '<div class="alert alert-success">Setting successfully updated</div>'
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();

            Weblog::set('Failed to change settings');
            Log::info($th->getMessage());
            return redirect()->back()->with([
                'pesan' => '<div class="alert alert-danger">An error occurred, please try again</div>'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Umum  $umum
     * @return \Illuminate\Http\Response
     */
    public function destroy(Umum $umum)
    {
        //
    }
}
