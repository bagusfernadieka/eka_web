<?php

namespace App\Http\Controllers;

use App\Facade\Weblog;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        if (Auth::check()) {
            return redirect(route('dashboard'));
        }

        $validasi = [
            'username' => 'required',
            'password' => 'required',
        ];

        if ($request->method() === 'POST') {
            if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'status' => true])) {

                Weblog::set('Login');
                return redirect(route('dashboard'));
            } else {
                return redirect('/')->with([
                    'pesan' => '<div class="alert alert-danger">Your username or password is incorrect!</div>'
                ]);
            }
        }

        $validator = JsValidatorFacade::make($validasi);
        return view('backend.auth.login', compact('validator'));
    }

    public function force_login($id)
    {
        $loginID = Auth::user()->id;
        $loginName = Auth::user()->nama;
        $loginUsername = Auth::user()->username;

        $user = User::find($id)->first();
        Weblog::set('Force login : ' . $user->username);

        if (Auth::loginUsingId($id)) {
            session([
                'loginAs' => true,
                'loginID' => $loginID,
                'loginName' => $loginName,
                'loginUsername' => $loginUsername,
            ]);

            return redirect(route('dashboard'));
        } else {
            return redirect('/')->with([
                'pesan' => '<div class="alert alert-danger">Your username or password is incorrect!</div>'
            ]);
        }
    }

    public function back_login($id)
    {
        Weblog::set('Exit from login As : ' . session('loginName'));

        if (Auth::loginUsingId(session('loginID'))) {
            session()->forget(['loginAs', 'loginID', 'loginName', 'loginUsername']);

            return redirect(route('dashboard'));
        }
    }

    public function logout()
    {
        Weblog::set('Logout');
        Auth::logout();
        return redirect(route('login'));
    }
}
