<?php

namespace App\Http\Controllers;

use App\Exports\CompanyExport;
use App\Facade\Weblog;
use App\Models\Company;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;
use Laratrust\LaratrustFacade as Laratrust;
use Maatwebsite\Excel\Facades\Excel;
use Proengsoft\JsValidation\Facades\JsValidatorFacade as JSvalidation;
use Intervention\Image\Facades\Image;
use App\Models\User;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:company-read')->only('index');
        $this->middleware('permission:company-create')->only(['create', 'store']);
        $this->middleware('permission:company-update')->only(['edit', 'update']);
        $this->middleware('permission:company-delete')->only('delete');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.company.index');
    }

    public function ajax(Request $request)
    {
        // filter
        $cari = $request->cari;
        // $tipe = $request->tipe;

        // set query
        $data = Company::query()
            ->with('userinput')
            ->with('userupdate')
            ->when($cari, fn ($e, $cari) => $e->where(function ($e) use ($cari) {
                $e->where('company_code', 'like', '%' . $cari . '%')->orWhere('company_name', 'like', '%' . $cari . '%');
            }))
            ->where('status', $request->status)
            ->orderBy('id','asc');

        // if export
        if ($request->method() === 'POST' && $request->filled('export')) {
            return Excel::download(new CompanyExport($data->get()), 'COMPANY.xlsx');
        }
        // dd($data->get()->toArray());

        // create datatable
        return DataTables::eloquent($data)
            ->addColumn('logo_mod', function ($e) {
                $logo = ($e->logo === "" || $e->logo === null) ? '/storage/company/photo.png' : '/storage/company/' . $e->logo;
                return '<a href="' . url($logo) . '" data-title="' . $e->company_name . '" style="height:50px; overflow:hidden; border-radius:4px;" class="detail-foto"><img src="' . url($logo) . '" class="rounded" width="40"/></a>';
            })
            ->addColumn('time_input_mod', function ($e) {
                $timeInputMod = '
                    <div class="d-flex align-items-center">
                        <b class="text-muted">'. Carbon::parse($e->time_input)->isoFormat('DD MMM YYYY HH:mm') .'</b>
                    </div>
                    <span class="text-muted font-12">By ' . strtoupper($e->userinput->nama) . '</span>
                ';
                return $timeInputMod;
            })
            ->addColumn('time_update_mod', function ($e) {
                if( $e->userupdate ) {
                    $timeUpdateMod = '
                        <div class="d-flex align-items-center">
                            <b class="text-muted">'. Carbon::parse($e->time_update)->isoFormat('DD MMM YYYY HH:mm') .'</b>
                        </div>
                        <span class="text-muted font-12">By ' . strtoupper($e->userupdate->nama) . '</span>
                    ';
                    return $timeUpdateMod;
                } else {
                    return '-';
                }
            })
            ->addColumn('aksi', function ($e) {
                $btnEdit = Laratrust::isAbleTo('company-update') ? '<a href="' . route('company.edit', ['company' => $e->id]) . '" class="btn btn-xs "><i class="bx bx-edit"></i></a>' : '';
                $btnDelete = Laratrust::isAbleTo('company-delete') ?  '<a href="' . route('company.destroy', ['company' => $e->id]) . '" data-title="' . $e->company_name . '" class="btn btn-xs text-danger btn-hapus"><i class="bx bx-trash"></i></a>' : '';
                $btnReload = Laratrust::isAbleTo('company-update') ? '<a href="' . route('company.destroy', ['company' => $e->id]) . '" data-title="' . $e->company_name . '" data-status="' . $e->status . '" class="btn btn-outline-secondary btn-xs btn-hapus"><i class="bx bx-refresh"></i></i></a>' : '';

                if ($e->status == 'y') {
                    return $btnEdit . ' ' . $btnDelete;
                } else {
                    return $btnReload;
                }
            })
            ->rawColumns(['logo_mod','time_input_mod','time_update_mod','aksi'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // validate form
        $validator = JSvalidation::make([
            'code'  => 'required|unique:companies,company_code|min:1',
            'name'  => 'required|min:3',
            'foto' => 'nullable',
        ]);

        // page
        return view('backend.company.create', compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate form
        $request->validate([
            'code'  => 'required|unique:companies,company_code|min:1',
            'name'  => 'required|min:3',
            'foto' => 'nullable',
        ]);

        // collect data
        $record = [
            'company_code' => $request->code,
            'company_name' => $request->name,
            'logo' => $request->foto,
            'user_input' => Auth::id(),
        ];

        DB::beginTransaction();
        try {
            // insert data
            Company::create($record);
            DB::commit();

            // create log
            Weblog::set('Create Company : ' . $request->name);

            // redirect index page
            return redirect(route('company.index'))->with(['pesan' => '<div class="alert alert-success">Data successfully created</div>']);
        } catch (\Throwable $th) {
            // error handle
            DB::rollBack();
            Log::info($th->getMessage());
            return redirect()->back()->with(['pesan' => '<div class="alert alert-danger">An error occurred, please try again</div>']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        // validate form
        $validator = JSvalidation::make([
            'code'  => 'required|unique:companies,company_code|min:1',
            'name'  => 'required|min:3',
        ]);

        // page
        return view('backend.company.edit', compact('validator', 'company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        // validate form
        $validasi = $request->validate([
            'code'  => 'required|min:1|unique:companies,company_code,' . $company->id,
            'name'  => 'required|min:3',
        ]);

        DB::beginTransaction();
        try {
            // update data
            Company::find($company->id)->update([
                'company_code' => $request->code,
                'company_name' => $request->name,
                'logo' => $request->foto,
                'user_update' => Auth::id(),
            ]);
            DB::commit();

            // create log
            Weblog::set('Update company name ' . $company->company_name . ' to ' . $request->name);

            // redirect index
            return redirect(route('company.index'))->with(['pesan' => '<div class="alert alert-success">Data successfully updated</div>']);
        } catch (\Throwable $th) {
            // error handle
            DB::rollBack();
            Log::warning($th->getMessage());
            return redirect()->back()->with(['pesan' => '<div class="alert alert-danger">An error occurred, please try again</div>']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        DB::beginTransaction();
        try {
            // delete
            $status = Company::find($company->id)->status;
            if ($status == 'y') {
                Company::find($company->id)->update([
                    'status' => 'n'
                ]);
                DB::commit();
                Weblog::set('Disable Company : ' . $company->company_name);

                return response()->json([
                    'tipe' => true,
                    'pesan' => 'Data successfully deleted'
                ]);
            } else {
                // activate
                Company::find($company->id)->update([
                    'status' => 'y'
                ]);
                DB::commit();
                Weblog::set('Activate Company : ' . $company->company_name);

                return response()->json([
                    'tipe' => true,
                    'pesan' => 'Data successfully reactivated'
                ]);
            }
        } catch (\Throwable $e) {
            DB::rollBack();
            return response()->json([
                'tipe' => false,
                'pesan' => $e->errorInfo,
            ]);
        }
    }

    public function ganti_foto(Request $request)
    {
        if ($request->has('file')) {
            $file = $request->file;
            $request->validate([
                'file' => 'required|image|max:2000'
            ]);

            $name = time();
            $ext  = $file->getClientOriginalExtension();
            $foto = $name . '.' . $ext;

            $path = $file->getRealPath();
            $thum = Image::make($path)->resize(80, 80, function ($size) {
                $size->aspectRatio();
            });
            $thumPath = public_path('/storage/company') . '/thum_' . $foto;
            $thum = Image::make($thum)->save($thumPath);

            $request->file->storeAs('public/company', $foto);

            return response()->json([
                'file' => $foto,
            ]);
        }
    }

    public function simpan_foto(Request $request)
    {
        DB::beginTransaction();
        try {
            User::find(Auth::id())->update(['foto' => $request->foto]);
            DB::commit();

            Weblog::set('Update Profile Photo');

            return response()->json([
                'pesan' => 'Photo successfully updated'
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response([
                'pesan' => 'An error occurred'
            ]);
        }
    }
}
