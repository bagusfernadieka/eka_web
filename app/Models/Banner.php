<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'banner';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function userinput()
    {
        return $this->belongsTo(User::class, 'user_input');
    }
    public function userupdate()
    {
        return $this->belongsTo(User::class, 'user_update');
    }
}
