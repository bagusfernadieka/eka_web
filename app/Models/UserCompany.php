<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCompany extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'user_company';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function userinput()
    {
        return $this->belongsTo(User::class, 'user_input');
    }
}
