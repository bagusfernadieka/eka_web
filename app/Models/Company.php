<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'companies';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function userinput()
    {
        return $this->belongsTo(User::class, 'user_input');
    }
    public function userupdate()
    {
        return $this->belongsTo(User::class, 'user_update');
    }
}
