<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'about';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function userinput()
    {
        return $this->belongsTo(User::class, 'user_input');
    }
    public function userupdate()
    {
        return $this->belongsTo(User::class, 'user_update');
    }
}
