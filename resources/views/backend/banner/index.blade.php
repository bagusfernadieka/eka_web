@extends('backend.layouts.layout')

@section('konten')
    <div class="container-xxl flex-grow-1 container-p-y">

        <div class="card mb-4">
            <h5 class="card-header">banner
                {!! statusBtn() !!}
            </h5>
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-sm-3 mt-2"><input type="text" id="cari" class="form-control" placeholder="Search..." autocomplete="off">
                    </div>
                    {{-- <div class="col-sm-2">
                        <button type="button" class="btn btn-outline-dark btn-sm mt-2" data-bs-toggle="modal"
                            data-bs-target="#filterModal">
                            <i class='bx bx-slider float-start me-1'></i>
                            Filter
                        </button>
                    </div> --}}
                    <div class="col-sm-9 mt-2">
                        @permission('banner-create')
                            <a href="{{ route('banner.create') }}" class="btn btn-sm btn-primary float-end">Create</a>
                        @endpermission
                    </div>
                </div>

                @if (session()->has('pesan'))
                    {!! session('pesan') !!}
                @endif

                <table class="table table-sm table-hover display nowrap mb-4" id="datatable">
                    <thead>
                        <tr>
                            <th>image</th>
                            <th>name</th>
                            <th>time input</th>
                            <th>last update</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {{-- modal gambar --}}
    <div class="modal modal-gambar fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title modal-title-gambar"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body modal-foto">
                </div>
            </div>
        </div>
    </div>

    {{-- <!-- FILTER Modal -->
    <div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="filterModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="filterModalLabel">Additional Filters</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-2">
                        <label for="jenis">Produk Jenis</label>
                        <select name="jenis" id="jenis" class="form-control select2" multiple>
                            <option value="internal">Internal</option>
                            <option value="kompetitor">Kompetitor</option>
                        </select>
                    </div>
                    <div class="mb-2">
                        <label for="kategori">Kategori</label>
                        <select name="kategori" id="kategori" class="form-control kategori-select"
                            data-ajax--url="{{ route('drop-kategori') }}" multiple></select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm"
                        onclick="datatables.ajax.reload()">Apply</button>
                </div>
            </div>
        </div>
    </div> --}}
@endsection

@section('script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap5.min.css">
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js"></script>

    <script>
        var datatables = $('#datatable').DataTable({
            scrollX: true,
            processing: true,
            serverSide: true,
            searching: false,
            lengthChange: false,
            pageLength: 10,
            bDestroy: true,
            ordering: false,
            ajax: {
                url: "{{ route('ajax-banner') }}",
                type: "POST",
                data: function(d) {
                    d._token = $("input[name=_token]").val();
                    d.status = $('.btn-check:checked').val();
                    d.cari = $('#cari').val();
                    // d.tipe = $('#tipe').val();
                },
            },
            columns: [
                {
                    data: 'image_mod'
                },
                {
                    data: 'banner_name'
                },
                {
                    data: 'time_input_mod'
                },
                {
                    data: 'time_update_mod'
                },
                {
                    data: 'aksi'
                },
            ]
        });

        $('#cari').keyup(function() {
            datatables.search($('#cari').val()).draw();
        });

        $(document).ready(function() {
            $('#datatable').on('click', '.detail-foto', function(e) {
                e.preventDefault();
                const gambar = $(this).attr('href');
                const title = $(this).data('title');

                $('.modal-gambar').modal('show');
                $('.modal-title-gambar').text(title);

                const foto = `<img src="${gambar}" class="img-fluid rounded" style="width:100%"/>`;
                $('.modal-foto').html(`${foto}`);
            })
        });
    </script>
@endsection
