<table>
    <thead>
        <tr>
            <th><b>NO</b></th>
            <th><b>COMPANY CODE</b></th>
            <th><b>COMPANY NAME</b></th>
            <th><b>TIME INPUT</b></th>
            <th><b>USER INPUT</b></th>
            <th><b>LAST UPDATE</b></th>
            <th><b>USER UPDATE</b></th>
        </tr>
    </thead>
    <tbody>
        @php
            $no=1;
        @endphp
        @foreach ($data as $item)
        <tr>
            <td align="center">{{ $no++ }}</td>
            <td>{{ $item->company_code }}</td>
            <td>{{ $item->company_name }}</td>
            <td>{{ $item->time_input }}</td>
            <td>{{ $item->userinput->nama }}</td>
            <td>{{ ($item->time_update == null) ? '-' : $item->time_update }}</td>
            <td>{{ ($item->userupdate == null) ? '-' : $item->userupdate->nama }}</td>
        @endforeach
    </tbody>
</table>
