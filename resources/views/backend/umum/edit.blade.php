@extends('backend.layouts.layout')

@section('konten')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
          <div class="col-md-6">
            <div class="card mb-4">
              <h5 class="card-header">Edit user</h5>
              <div class="card-body">
                @if (session()->has('pesan'))
                    {!! session('pesan') !!}
                @endif

                <form action="{{ route('umum.update', ['umum' => $umum->id]) }}" method="POST" enctype="multipart/form-data" id="my-form">
                    @csrf
                    @method('PATCH')

                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label" for="nama">Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama" value="{{ $umum->nama }}">
                        </div>
                    </div>

                    {{-- LOGO --}}
                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label" for="email">Logo</label>
                        <div class="col-sm-9">
                            <div class="button-wrapper">
                                <button type="button" class="account-file-input btn btn-sm btn-outline-primary" data-bs-toggle="modal" data-bs-target="#modalUploadFoto">
                                    <span class="d-none d-sm-block">Change logo</span>
                                    <i class="bx bx-upload d-block d-sm-none"></i>
                                </button>
                                <input type="hidden" name="logo" id="foto" value="">
                                <div><small class="text-muted mb-0">JPG, GIF, PNG. Maximum size 2000 Kb</small></div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label" for="email"></label>
                        <div class="col-sm-9">
                            <div id="box-foto">
                                @if ($umum->logo === 'logo' || $umum->logo === NULL || $umum->logo == '')
                                    The logo has not been set
                                @else
                                <img src="{{ url('/storage/foto/thum_'.$umum->logo) }}" class="rounded">
                                @endif
                            </div>
                        </div>
                    </div>

                    {{-- FAVICON --}}
                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label" for="email">Favicon</label>
                        <div class="col-sm-9">
                            <div class="button-wrapper">
                                <button type="button" class="account-file-input btn btn-sm btn-outline-primary" data-bs-toggle="modal" data-bs-target="#modalUploadFavicon">
                                    <span class="d-none d-sm-block">Change favicon</span>
                                    <i class="bx bx-upload d-block d-sm-none"></i>
                                </button>
                                <input type="hidden" name="favicon" id="favicon" value="">
                                <div><small class="text-muted mb-0">JPG, GIF, PNG. Maximum size 2000 Kb</small></div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label" for="email"></label>
                        <div class="col-sm-9">
                            <div id="box-favicon">
                                @if ($umum->favicon === 'favicon' || $umum->favicon === NULL || $umum->favicon == '')
                                    The favicon has not been set
                                @else
                                <img src="{{ url('/storage/foto/thum_'.$umum->favicon) }}" class="rounded">
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-end">
                      <div class="col-sm-9">
                        <a href="{{ route('umum.index') }}" class="btn btn-link btn-sm">Cancel</a>
                        <button type="submit" class="btn btn-primary btn-sm">Save</button>
                      </div>
                    </div>
                </form>
              </div>
              <!-- /Account -->
            </div>

          </div>
        </div>
      </div>


    <!-- Modal LOGO-->
    <div class="modal fade" id="modalUploadFoto" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalUploadFotoLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalUploadFotoLabel">Upload logo</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <span id="notif"></span>
                <form action="{{ route('ganti-foto') }}" class="dropzone" id="upload-image" method="POST" enctype="multipart/form-data">
                    @csrf
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-sm btn-simpan" onclick="simpanFoto()">Add</button>
            </div>
        </div>
        </div>
    </div>

    <!-- Modal FAVICON-->
    <div class="modal fade" id="modalUploadFavicon" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalUploadFaviconLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalUploadFaviconLabel">Upload favicon</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <span id="notif-favicon"></span>
                <form action="{{ route('ganti-foto') }}" class="dropzone" id="upload-favicon" method="POST" enctype="multipart/form-data">
                    @csrf
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-sm btn-simpan" onclick="simpanFoto('favicon')">Add</button>
            </div>
        </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! $validator->selector('#my-form') !!}

    <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />

    <script>
        // ------------- Logo
        Dropzone.options.uploadImage = {
            maxFilesize : 2000,
            acceptedFiles: ".jpeg,.jpg,.png",
            method : 'post',
            createImageThumbnails: true,
            init: function(){
                this.on("addedfile", file => {
                    $('.btn-simpan').attr('disabled','disabled').text('Loading...');
                });
            },
            success: function (file, response) {
                $('.btn-simpan').removeAttr('disabled').text('Add');
                const foto = response.file;
                $('.modal-body #notif').html(`<div class="alert alert-success">Photo successfully uploaded</div>`);
                $('#foto').val(foto);
            },
            error: function(file, response){
                $('.btn-simpan').removeAttr('disabled').text('Add');
                const pesan = response.message;
                $('.modal-body #notif').html(`<div class="alert alert-danger">${pesan}</div>`);
            }
        };

        // ------------- Favicon
        Dropzone.options.uploadFavicon = {
            maxFilesize : 2000,
            acceptedFiles: ".jpeg,.jpg,.png",
            method : 'post',
            createImageThumbnails: true,
            init: function(){
                this.on("addedfile", file => {
                    $('.btn-simpan').attr('disabled','disabled').text('Loading...');
                });
            },
            success: function (file, response) {
                $('.btn-simpan').removeAttr('disabled').text('Add');
                const foto = response.file;
                $('.modal-body #notif-favicon').html(`<div class="alert alert-success">Favicon successfully uploaded</div>`);
                $('#favicon').val(foto);
            },
            error: function(file, response){
                $('.btn-simpan').removeAttr('disabled').text('Add');
                const pesan = response.message;
                $('.modal-body #notif-favicon').html(`<div class="alert alert-danger">${pesan}</div>`);
            }
        };

        function simpanFoto($tipe = ''){
            let title= '';
            let foto = '';
            let boxImage = '';
            let notif = '';

            if($tipe == ''){
                title= 'Logo';
                foto = $('#foto').val();
                boxImage = $('#box-foto');
                notif = $('#notif');
            }else{
                title= 'Favicon';
                foto = $('#favicon').val();
                boxImage = $('#box-favicon');
                notif = $('#notif-favicon');
            }

            if(foto === '' || foto === null){
                $(notif).html(`<div class="alert alert-danger">Unable to add ${title}</div>`);
            }else{
                $('#modalUploadFoto, #modalUploadFavicon').modal('hide');
                $(boxImage).html(`<img src="{{ url('/storage/foto/thum_${foto}') }}" class="rounded">`);
            }
        }
    </script>
@endsection
