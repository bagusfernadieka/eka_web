@extends('backend.layouts.layout')

@section('konten')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
          <div class="col-md-6">
            <div class="card mb-4">
              <h5 class="card-header">Setting</h5>
              <div class="card-body">
                @if (session()->has('pesan'))
                    {!! session('pesan') !!}
                @endif

                    <div class="row mb-3">
                      <label class="col-sm-3 col-form-label" for="nama">Name</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="username" value="{{ $pengaturan->nama }}" disabled>
                      </div>
                    </div>

                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label" for="nama">Logo</label>
                        <div class="col-sm-9">
                            @if ($pengaturan->logo === 'logo' || $pengaturan->logo === NULL || $pengaturan->logo == '')
                              The logo has not been set
                            @else
                                <img src="{{ url('/storage/foto/thum_'.$pengaturan->logo) }}" alt="">
                            @endif
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label" for="nama">Favicon</label>
                        <div class="col-sm-9">
                            @if ($pengaturan->logo === 'logo' || $pengaturan->logo === NULL || $pengaturan->logo == '')
                              The Favicon has not been set
                            @else
                                <img src="{{ url('/storage/foto/'.$pengaturan->favicon) }}" alt="">
                            @endif
                        </div>
                    </div>

                    @permission('umum-update')
                    <div class="row justify-content-end">
                      <div class="col-sm-9">
                        <a href="{{ route('umum.edit', ['umum' => $pengaturan->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                      </div>
                    </div>
                    @endpermission

              </div>
              <!-- /Account -->
            </div>

          </div>
        </div>
      </div>

@endsection

