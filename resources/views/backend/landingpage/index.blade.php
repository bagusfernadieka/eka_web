<!DOCTYPE html>
<html
    lang="en"
    class="light-style customizer-hide"
    dir="ltr"
    data-theme="theme-default"
    data-assets-path="{{ asset('backend/sneat-1.0.0assets/') }}"
    data-template="vertical-menu-template-free"
>
    <head>
        <meta charset="utf-8" />
        <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
        />

        <title>{{ $title_web }}</title>

        <meta name="description" content="" />

        <!-- Favicon -->
        <link rel="icon" type="image/x-icon" href="{{ $favicon }}" />
        <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet"
        />

        <!-- =========================
            Loding All Stylesheet
        ============================== -->
        <link rel="stylesheet" href="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/css/hover-min.css">
        <link rel="stylesheet" href="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/css/magnific-popup.css">
        <link rel="stylesheet" href="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/css/flexslider.css">
        <link rel="stylesheet" href="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/css/bootstrap.min.css">

        <!-- =========================
            Loding Main Theme Style
        ============================== -->
        <link rel="stylesheet" href="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/css/style.css">
        <link rel="stylesheet" href="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/css/color.css">

        <!-- =========================
            Header Loding JS Script
        ============================== -->
        <script src="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/js/modernizr.js"></script>

    </head>

    <body id="vigo_body">
        <div class="vigo-loder">
            <div class="sk-rotating-plane"></div>
        </div>

        <!-- ==============================
            Main Navbar
        =================================== -->
        <section class="main-nav bg-white" id="sticker">
            <div class="container custom-container">
                <nav class="navbar-expand-lg navbar-light d-lg-flex justify-content-lg-end pos-r">
                    <div class="main-logo text-center d-lg-inline-flex justify-content-start d-lg-flex align-items-lg-center">
                        <a href="{{ route('/') }}" class="app-brand-link gap-2">
                            <h2 class="">{!! $logo_landing !!}</h2>
                        </a>
                    </div>
                    <div class="d-flex justify-content-between d-lg-inline-flex justify-content-lg-between">
                        <div class="p-2">
                            <button class="navbar-toggler cursor-pointer focus-none" type="button" data-toggle="collapse" data-target="#header-nav-btn" aria-controls="header-nav-btn" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="header-nav-btn">
                                <div class="navbar-nav mr-auto mt-2 mt-lg-0"></div>
                                <div class="my-2 my-lg-0">
                                    <ul class="navbar-nav" id="nav">
                                        <li class="nav-item">
                                            <a class="nav-link lato" href="#vigo_body">Home</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link lato" href="#footer">Contact</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="p-2 d-lg-flex align-items-lg-center">
                            <a href="{{ route('login') }}" class="btn ral btn-outline-primary vigo-btn hvr-shutter-out-horizontal bg-green border-transparent white focus-none">{{ auth()->check() ? 'Dashboard' : 'Login' }}</a>
                        </div>
                    </div>
                </nav>     
            </div>
        </section>
    
        <!-- ==============================
            Slider Section
        =================================== -->
        <section class="main-slider">
            <div id="main-slider" class="owl-carousel owl-theme">
                @foreach ($banner as $item)
                    <div class="item">
                        <img src="{{ asset('storage/banner') . '/' . $item->image }}" class="img-fluid" alt="{{ $item->banner_name }}">
                    </div>    
                @endforeach
            </div>
        </section>

        <div class="p-5">
            <h4>Produk Terbaru</h4>
            <div class="row">
                @foreach ($product as $item)
                    <div class="col">
                        <div class="card bluesky text-white">
                            <div class="card-body">
                                <img src="{{ asset('storage/product') . '/' . $item->photo }}" class="img-fluid" alt="{{ $item->name }}">
                                <h6 class="card-title text-secondary">{{ $item->name }}</h6>
                                <p class="card-text text-dark">{{ $item->price }}</p>
                            </div>
                        </div>
                    </div>  
                @endforeach
            </div>
        </div>

        <div class="p-5">
            <h4>Produk Tersedia</h4>
            <div class="row">
                @foreach ($product as $item)
                    <div class="col">
                        <div class="card bluesky text-white">
                            <div class="card-body">
                                <img src="{{ asset('storage/product') . '/' . $item->photo }}" class="img-fluid" alt="{{ $item->name }}">
                                <h6 class="card-title text-secondary">{{ $item->name }}</h6>
                                <p class="card-text text-dark">{{ $item->price }}</p>
                            </div>
                        </div>
                    </div>  
                @endforeach
            </div>
        </div>
    
        <!-- ==============================
            footer Section
        =================================== -->
        <footer id="footer">
            <div class="container custom-container">
                <div class="row d-flex justify-content-around">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
                        <div class="footer-box white">
                            <h4 class="footer-title pos-r">
                                About Us
                            </h4>
                            <p class="footer-content">
                                {{ $about->about }}
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1 d-none d-xl-block footer-box-border"></div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
                        <div class="footer-box white mr-0">
                            <h4 class="footer-title pos-r">
                                Contact Us
                            </h4>
                            <div class="contact-address">
                                <ul class="list-group white">
                                    <li class="list-group-item pd-0 bg-transparent border-transparent">
                                        <a href="tel:{{ $about->phone }}" class="white"><i class="fa fa-phone" aria-hidden="true"></i> {{ $about->phone }}</a>
                                    </li>
                                    <li class="list-group-item pd-0 bg-transparent border-transparent">
                                        <a href="mailto:{{ $about->phone }}" class="white"><i class="fa fa-envelope" aria-hidden="true"></i> {{ $about->phone }}</a>
                                    </li>
                                    <li class="list-group-item pd-0 bg-transparent border-transparent d-flex align-items-start border-0">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <p class="display-inline-block mr-0">{{ $about->address }}</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="copyright bg-black">
            <div class="container custom-container text-center">
                <div class="row">
                    <div class="col-12 col-md-8 d-xl-flex justify-content-start">
                        <div class="copyright-box">
                            <p class="copyright-text white">
                                Copyright &copy; <a href="https://fernadi.my.id" class="white" target="_blank">Fernadi</a>  {{ date('Y') }}. All Right Reserved
                            </p>
                        </div>
                    </div>
                    <div class="col-12 d-flex d-md-flex justify-content-md-end">
                        <a href="" class="back_to_top">
                            <i class="fa fa-angle-double-up"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    
    
        <!-- =========================
            Main Loding JS Script
        ============================== -->
        <script src="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/js/jquery.min.js"></script>
        <script src="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/js/popper.min.js"></script>
        <script src="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/js/bootstrap.min.js"></script>
        <script src="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/js/simplePlayer.js"></script>
        <script src="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/js/owl.carousel.min.js"></script>
        <script src="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/js/jquery.flexslider.js"></script>
        <script src="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/js/jquery.magnific-popup.min.js"></script>
        <script src="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/js/jquery.sticky.js"></script>
        <script src="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/js/jquery.nav.js"></script>
        <script src="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/js/mixitup.min.js"></script>
        <script src="{{ asset('backend/sneat-1.0.0') }}/landing_page_assets/js/main.js"></script>
    
    
    </body>
</html>
