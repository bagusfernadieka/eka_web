@extends('backend.layouts.layout')

@section('konten')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-12">
                <div class="mb-4">
                    <h5 class="card-header">dashboard
                    </h5>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="card bluesky text-white">
                                    <div class="card-body">
                                        <p class="card-title text-secondary">Jumlah User</p>
                                        <h3 class="card-text">150 <span class="font-18"> User</span></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card bluesky text-white">
                                    <div class="card-body">
                                        <p class="card-title text-secondary">Jumlah User Aktif</p>
                                        <h3 class="card-text">150 <span class="font-18"> User</span></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card bluesky text-white">
                                    <div class="card-body">
                                        <p class="card-title text-secondary">Jumlah Produk</p>
                                        <h3 class="card-text">150 <span class="font-18"> Produk</span></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card bluesky text-white">
                                    <div class="card-body">
                                        <p class="card-title text-secondary">Jumlah Produk Aktif</p>
                                        <h3 class="card-text">150 <span class="font-18"> Produk</span></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="card mb-4">
                    <h5 class="card-header">New Product
                    </h5>
                    <div class="card-body">
                        <div class="row mb-4">
                            <div class="col-sm-3 mt-2"><input type="text" id="cari" class="form-control" placeholder="Search..." autocomplete="off">
                            </div>
                        </div>
                        <table class="table table-sm table-hover display nowrap mb-4" id="datatable">
                            <thead>
                                <tr>
                                    <th>photo</th>
                                    <th>product</th>
                                    <th>time input</th>
                                    <th>price (Rp)</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- modal gambar --}}
    <div class="modal modal-gambar fade" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title modal-title-gambar"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body modal-foto">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap5.min.css">
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js"></script>

    <script>
        var datatables = $('#datatable').DataTable({
            scrollX: true,
            processing: true,
            serverSide: true,
            searching: false,
            lengthChange: false,
            pageLength: 10,
            bDestroy: true,
            ordering: false,
            ajax: {
                url: "{{ route('ajax-dashboard') }}",
                type: "POST",
                data: function(d) {
                    d._token = $("input[name=_token]").val();
                    d.cari = $('#cari').val();
                },
            },
            columns: [
                {
                    data: 'photo_mod'
                },
                {
                    data: 'name'
                },
                {
                    data: 'time_input_mod'
                },
                {
                    data: 'price_mod'
                },
            ]
        });

        $('#cari').keyup(function() {
            datatables.search($('#cari').val()).draw();
        });

        $(document).ready(function() {
            $('#datatable').on('click', '.detail-foto', function(e) {
                e.preventDefault();
                const gambar = $(this).attr('href');
                const title = $(this).data('title');

                $('.modal-gambar').modal('show');
                $('.modal-title-gambar').text(title);

                const foto = `<img src="${gambar}" class="img-fluid rounded" style="width:100%"/>`;
                $('.modal-foto').html(`${foto}`);
            })
        });
    </script>
@endsection