<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="{{ route('dashboard') }}" class="app-brand-link">
            <h3 class="">{!! $logo !!}</h3>
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>

    {{-- <div class="menu-inner-shadow"></div> --}}

    <ul class="menu-inner py-1 mt-2">
        <!-- Dashboard -->
        <li class="menu-item {{ menuAktif('dashboard') }}">
            <a href="{{ route('dashboard') }}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-pie-chart-alt-2"></i>
                <div data-i18n="Analytics">Dashboard</div>
            </a>
        </li>

        <!-- Master Data -->
        {{-- <li class="menu-header small text-uppercase"><span class="menu-header-text">Master Data</span></li> --}}
        @permission('product-read')
            <li class="menu-item {{ menuAktif('product') }}">
                <a href="{{ route('product.index') }}" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-package"></i>
                    <div data-i18n="Analytics">Product</div>
                </a>
            </li>
        @endpermission

        @permission('company-read')
            <li class="menu-item {{ menuAktif('company') }}">
                <a href="{{ route('company.index') }}" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-buildings"></i>
                    <div data-i18n="Analytics">Company</div>
                </a>
            </li>
        @endpermission

        @permission('user-read', 'role-read', 'permission-read')
            {{-- <li class="menu-header small text-uppercase"><span class="menu-header-text">Web Settings</span></li> --}}
            <li class="menu-item {{ menuAktif(['user', 'role', 'permission']) }}">
                <a href="#" class="menu-link menu-toggle">
                    <i class='menu-icon tf-icons bx bx-user-circle'></i>
                    <div data-i18n="Form Layouts">Userweb</div>
                </a>
                <ul class="menu-sub">
                    @permission('user-read')
                        <li class="menu-item {{ menuAktif('user') }}">
                            <a href="{{ route('user.index') }} " class="menu-link">
                                <div data-i18n="Vertical Form">User</div>
                            </a>
                        </li>
                    @endpermission
                    @permission('role-read')
                        <li class="menu-item {{ menuAktif('role') }}">
                            <a href="{{ route('role.index') }}" class="menu-link">
                                <div data-i18n="Horizontal Form">Role</div>
                            </a>
                        </li>
                    @endpermission
                    @permission('permission-read')
                        <li class="menu-item {{ menuAktif('permission') }}">
                            <a href="{{ route('permission.index') }}" class="menu-link">
                                <div data-i18n="Horizontal Form">Permission</div>
                            </a>
                        </li>
                    @endpermission
                </ul>
            </li>
        @endpermission

        @permission('banner-read')
            <li class="menu-item {{ menuAktif('banner') }}">
                <a href="{{ route('banner.index') }}" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-tab"></i>
                    <div data-i18n="Analytics">Banner</div>
                </a>
            </li>
        @endpermission

        @permission('about-read')
            <li class="menu-item {{ menuAktif('about') }}">
                <a href="{{ route('about.index') }}" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-info-circle"></i>
                    <div data-i18n="Analytics">About</div>
                </a>
            </li>
        @endpermission

        @permission('umum-read')
            <li class="menu-item {{ menuAktif('umum') }}">
                <a href="{{ route('umum.index') }}" class="menu-link">
                    <i class="menu-icon tf-icons bx bx-cog"></i>
                    <div data-i18n="Analytics">Setting</div>
                </a>
            </li>
        @endpermission


    </ul>
</aside>
