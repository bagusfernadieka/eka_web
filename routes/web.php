<?php

use App\Http\Controllers\LandingPageController;
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UmumController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('xss')->group(function () {

    // Route::get('/buat-kalender', [ParsingController::class, 'buat_kalender']);
    Route::any('/', [LandingPageController::class, 'index'])->name('/');
    Route::any('/login', [LoginController::class, 'index'])->name('login');

    Route::middleware(['auth'])->group(function () {
        Route::prefix('auth')->group(function () {
            Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');
            Route::post('/ajax-dashboard', [HomeController::class, 'ajax'])->name('ajax-dashboard');
            Route::any('/profil', [UserController::class, 'profil'])->name('profil');
            Route::post('/ganti-foto', [UserController::class, 'ganti_foto'])->name('ganti-foto');
            Route::post('/simpan-foto', [UserController::class, 'simpan_foto'])->name('simpan-foto');
            Route::any('/password', [UserController::class, 'password'])->name('password');
            Route::post('/ganti-password', [UserController::class, 'ganti_password'])->name('ganti-password');
            Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
            Route::any('/weblog', [LogController::class, 'index'])->name('aktivitas');
            Route::get('/force-login/{id}', [LoginController::class, 'force_login'])->name('force-login');
            Route::get('/back-login/{id}', [LoginController::class, 'back_login'])->name('back-login');

            // Master Data
            // Product
            Route::resource('product', ProductController::class);
            Route::post('/ajax-product', [ProductController::class, 'ajax'])->name('ajax-product');
            Route::post('/product-ganti-foto', [ProductController::class, 'ganti_foto'])->name('product-ganti-foto');

            // Company
            Route::resource('company', CompanyController::class);
            Route::post('/ajax-company', [CompanyController::class, 'ajax'])->name('ajax-company');
            Route::post('/company-ganti-foto', [CompanyController::class, 'ganti_foto'])->name('company-ganti-foto');

            // Setting
            Route::resource('umum', UmumController::class);
            Route::resource('user', UserController::class);
            Route::post('/ajax-user', [UserController::class, 'ajax'])->name('ajax-user');

            Route::resource('role', RoleController::class);
            Route::post('/ajax-role', [RoleController::class, 'ajax'])->name('ajax-roles');
            Route::resource('permission', PermissionController::class);
            Route::post('/ajax-permission', [PermissionController::class, 'ajax'])->name('ajax-permission');

            // Banner
            Route::resource('banner', BannerController::class);
            Route::post('/ajax-banner', [BannerController::class, 'ajax'])->name('ajax-banner');
            Route::post('/banner-ganti-foto', [BannerController::class, 'ganti_foto'])->name('banner-ganti-foto');

            // about
            Route::resource('about', AboutController::class);

            // ajax
            Route::prefix('ajax')->group(function () {
                Route::post('/role', [AjaxController::class, 'role'])->name('drop-role');
                Route::post('/company', [AjaxController::class, 'company'])->name('drop-company');
                Route::post('/group', [AjaxController::class, 'group'])->name('drop-group');
                Route::post('/examtype', [AjaxController::class, 'examtype'])->name('drop-examtype');
                Route::post('/method', [AjaxController::class, 'method'])->name('drop-method');
                Route::post('/grade', [AjaxController::class, 'grade'])->name('drop-grade');
                Route::post('/fase', [AjaxController::class, 'fase'])->name('drop-fase');
                Route::post('/exam', [AjaxController::class, 'exam'])->name('drop-exam');
                Route::post('/room', [AjaxController::class, 'room'])->name('drop-room');
                Route::post('/surveillancetype', [AjaxController::class, 'surveillancetype'])->name('drop-surveillancetype');
            });
        });
    });
});
