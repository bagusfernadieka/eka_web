-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: eka_api
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `about`
--

DROP TABLE IF EXISTS `about`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `about` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `about` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_input` int NOT NULL,
  `time_input` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_update` int DEFAULT NULL,
  `time_update` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `about_users_FK` (`user_input`),
  KEY `about_users_FK_1` (`user_update`),
  CONSTRAINT `about_users_FK` FOREIGN KEY (`user_input`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `about_users_FK_1` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about`
--

LOCK TABLES `about` WRITE;
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
INSERT INTO `about` VALUES (1,'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Cum, atque, sunt vitae incidunt quisquam odit eum hic aliquam laborum dolor deleniti natus numquam beatae sint non explicabo voluptates libero quam in porro tenetur vero excepturi! Qui dolore vero voluptatum atque awe.','+62895632349517','bagusfernadieka@gmail.com','201 Oak Street Building\r\n27, Manchester, USA',1,'2024-03-07 16:01:04',1,'2024-03-07 16:57:13');
/*!40000 ALTER TABLE `about` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `banner` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `banner_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('y','n') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'y',
  `user_input` int NOT NULL,
  `time_input` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_update` int DEFAULT NULL,
  `time_update` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `banner_users_FK` (`user_input`),
  KEY `banner_users_FK_1` (`user_update`),
  CONSTRAINT `banner_users_FK` FOREIGN KEY (`user_input`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `banner_users_FK_1` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (1,'Slider 1','1710833647.jpg','y',1,'2024-03-07 15:58:14',1,'2024-03-19 14:34:09'),(2,'Slider 2','1710833663.jpg','y',1,'2024-03-07 15:58:14',1,'2024-03-19 14:34:25');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `companies` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `company_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('y','n') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'y',
  `user_input` int NOT NULL,
  `time_input` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_update` int DEFAULT NULL,
  `time_update` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `companies_unique` (`company_code`),
  KEY `companies_users_FK` (`user_input`),
  KEY `companies_users_FK_1` (`user_update`),
  CONSTRAINT `companies_users_FK` FOREIGN KEY (`user_input`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `companies_users_FK_1` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'101','Company A',NULL,'y',1,'2024-03-02 10:49:58',1,'2024-03-19 12:06:36'),(34,'102','Company B',NULL,'y',1,'2024-03-03 09:44:04',1,'2024-03-05 13:20:27'),(35,'103','Company C',NULL,'y',1,'2024-03-03 09:44:04',1,'2024-03-05 13:20:34');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_user`
--

DROP TABLE IF EXISTS `log_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `log_user` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `browser` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser_version` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `platform` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `platform_version` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_tipe` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log_user_FK` (`users_id`),
  CONSTRAINT `log_user_FK` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_user`
--

LOCK TABLES `log_user` WRITE;
/*!40000 ALTER TABLE `log_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `logs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `browser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser_version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `platform` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `platform_version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_tipe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `logs_FK` (`user_id`),
  CONSTRAINT `logs_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=406 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` VALUES (1,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:09:57','2024-02-21 09:09:57'),(2,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:10:21','2024-02-21 09:10:21'),(3,1,'Edit Role : superadmin','127.0.0.1','http://localhost:8000/auth/role/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:10:44','2024-02-21 09:10:44'),(4,1,'Membuat permission : tes','127.0.0.1','http://localhost:8000/auth/permission','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:11:15','2024-02-21 09:11:15'),(5,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:12:36','2024-02-21 09:12:36'),(6,1,'Menghapus permission : tes','127.0.0.1','http://localhost:8000/auth/permission/106','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:13:57','2024-02-21 09:13:57'),(7,1,'Edit permission : user','127.0.0.1','http://localhost:8000/auth/permission/61','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:14:54','2024-02-21 09:14:54'),(8,1,'Edit permission : umum','127.0.0.1','http://localhost:8000/auth/permission/56','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:16:38','2024-02-21 09:16:38'),(9,1,'Edit permission : role','127.0.0.1','http://localhost:8000/auth/permission/51','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:17:00','2024-02-21 09:17:00'),(10,1,'Edit permission : permission','127.0.0.1','http://localhost:8000/auth/permission/31','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:17:16','2024-02-21 09:17:16'),(11,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:17:57','2024-02-21 09:17:57'),(12,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:18:12','2024-02-21 09:18:12'),(13,1,'Edit Role : superadmin','127.0.0.1','http://localhost:8000/auth/role/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:22:38','2024-02-21 09:22:38'),(14,1,'Update profile','127.0.0.1','http://localhost:8000/auth/profil','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 09:53:53','2024-02-21 09:53:53'),(15,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 10:07:19','2024-02-21 10:07:19'),(16,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 16:03:33','2024-02-21 16:03:33'),(17,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 16:14:43','2024-02-21 16:14:43'),(18,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 16:29:36','2024-02-21 16:29:36'),(19,1,'Update profile','127.0.0.1','http://localhost:8000/auth/profil','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 16:30:35','2024-02-21 16:30:35'),(20,1,'Ganti password','127.0.0.1','http://localhost:8000/auth/password','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 16:32:18','2024-02-21 16:32:18'),(21,1,'Force login : admin','127.0.0.1','http://localhost:8000/auth/force-login/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 17:18:28','2024-02-21 17:18:28'),(22,2,'Exit from login As : superadmin','127.0.0.1','http://localhost:8000/auth/back-login/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 17:18:37','2024-02-21 17:18:37'),(23,1,'Create user asdf','127.0.0.1','http://localhost:8000/auth/user','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 17:23:34','2024-02-21 17:23:34'),(24,1,'Delete user : asdf','127.0.0.1','http://localhost:8000/auth/user/13','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 17:23:43','2024-02-21 17:23:43'),(25,1,'Update profile : adminweb','127.0.0.1','http://localhost:8000/auth/user/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 17:23:52','2024-02-21 17:23:52'),(26,1,'Membuat role : Cuba','127.0.0.1','http://localhost:8000/auth/role','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 17:27:36','2024-02-21 17:27:36'),(27,1,'Edit Role : Cuba','127.0.0.1','http://localhost:8000/auth/role/6','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 17:27:52','2024-02-21 17:27:52'),(28,1,'Delete Role : Cuba','127.0.0.1','http://localhost:8000/auth/role/6','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 17:28:13','2024-02-21 17:28:13'),(29,1,'Membuat permission : chat','127.0.0.1','http://localhost:8000/auth/permission','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 17:31:20','2024-02-21 17:31:20'),(30,1,'Edit permission : chata','127.0.0.1','http://localhost:8000/auth/permission/131','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 17:31:37','2024-02-21 17:31:37'),(31,1,'Delete permission : chata','127.0.0.1','http://localhost:8000/auth/permission/136','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-21 17:31:44','2024-02-21 17:31:44'),(32,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-22 09:11:25','2024-02-22 09:11:25'),(33,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-02-29 23:20:30','2024-02-29 23:20:30'),(34,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-02 02:38:27','2024-03-02 02:38:27'),(36,1,'Delete permission : company','127.0.0.1','http://localhost:8000/auth/permission/141','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-02 02:48:35','2024-03-02 02:48:35'),(43,1,'Edit Role : superadmin','127.0.0.1','http://localhost:8000/auth/role/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-02 02:52:26','2024-03-02 02:52:26'),(44,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-02 06:57:17','2024-03-02 06:57:17'),(45,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-02 22:24:15','2024-03-02 22:24:15'),(46,1,'Create Company : 2','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 00:57:13','2024-03-03 00:57:13'),(47,1,'Create Company : 3, 4, 5','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 01:11:19','2024-03-03 01:11:19'),(48,1,'Create Company : 6','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 01:11:37','2024-03-03 01:11:37'),(49,1,'Create Company : 7','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 01:31:10','2024-03-03 01:31:10'),(50,1,'Create Company : 8, 9, 10, 11','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 01:31:37','2024-03-03 01:31:37'),(51,1,'Create Company : 4, 5','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 01:57:40','2024-03-03 01:57:40'),(52,1,'Create Company : 6, 7','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 01:59:28','2024-03-03 01:59:28'),(53,1,'Create Company : 8, 9','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:07:58','2024-03-03 02:07:58'),(55,1,'Update company name Company C to Company D','127.0.0.1','http://localhost:8000/auth/company/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:24:23','2024-03-03 02:24:23'),(56,1,'Disable Company : Company D','127.0.0.1','http://localhost:8000/auth/company/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:32:50','2024-03-03 02:32:50'),(57,1,'Activate Company : Company D','127.0.0.1','http://localhost:8000/auth/company/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:33:11','2024-03-03 02:33:11'),(58,1,'Disable Company : Company D','127.0.0.1','http://localhost:8000/auth/company/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:37:15','2024-03-03 02:37:15'),(59,1,'Activate Company : Company D','127.0.0.1','http://localhost:8000/auth/company/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:37:30','2024-03-03 02:37:30'),(60,1,'Create Company : Company C','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:39:07','2024-03-03 02:39:07'),(61,1,'Create Company : Company E, Company F','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:39:37','2024-03-03 02:39:37'),(62,1,'Update company name Company F to Company F','127.0.0.1','http://localhost:8000/auth/company/33','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:41:19','2024-03-03 02:41:19'),(63,1,'Update company name Company F to Company B','127.0.0.1','http://localhost:8000/auth/company/33','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:41:33','2024-03-03 02:41:33'),(64,1,'Disable Company : Company B','127.0.0.1','http://localhost:8000/auth/company/33','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:41:43','2024-03-03 02:41:43'),(65,1,'Activate Company : Company B','127.0.0.1','http://localhost:8000/auth/company/33','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:42:06','2024-03-03 02:42:06'),(66,1,'Create Company : Company B, Company C','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:44:04','2024-03-03 02:44:04'),(67,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 02:54:04','2024-03-03 02:54:04'),(68,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 04:47:31','2024-03-03 04:47:31'),(69,1,'Create user admincrm1','127.0.0.1','http://localhost:8000/auth/user','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 05:54:52','2024-03-03 05:54:52'),(70,1,'Create user admincrm2','127.0.0.1','http://localhost:8000/auth/user','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 06:03:12','2024-03-03 06:03:12'),(71,1,'Update profile : admincrm1','127.0.0.1','http://localhost:8000/auth/user/19','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 06:53:39','2024-03-03 06:53:39'),(72,1,'Update profile : admincrm1','127.0.0.1','http://localhost:8000/auth/user/19','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 06:54:03','2024-03-03 06:54:03'),(73,1,'Update profile : AdminComC','127.0.0.1','http://localhost:8000/auth/user/19','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 06:55:14','2024-03-03 06:55:14'),(74,1,'Update profile : AdminComC','127.0.0.1','http://localhost:8000/auth/user/19','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 06:55:32','2024-03-03 06:55:32'),(75,1,'Create user superadmin','127.0.0.1','http://localhost:8000/auth/user','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 08:51:32','2024-03-03 08:51:32'),(76,1,'Update profile : admincrm2','127.0.0.1','http://localhost:8000/auth/user/21','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 08:51:55','2024-03-03 08:51:55'),(80,2,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 09:18:27','2024-03-03 09:18:27'),(81,2,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 09:18:44','2024-03-03 09:18:44'),(82,1,'Update profile : adminweb','127.0.0.1','http://localhost:8000/auth/user/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 09:20:27','2024-03-03 09:20:27'),(83,1,'Delete user : superadmin','127.0.0.1','http://localhost:8000/auth/user/22','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 09:36:12','2024-03-03 09:36:12'),(84,1,'Delete user : admincrm2','127.0.0.1','http://localhost:8000/auth/user/21','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 09:36:21','2024-03-03 09:36:21'),(85,1,'Delete user : AdminComC','127.0.0.1','http://localhost:8000/auth/user/19','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 09:36:29','2024-03-03 09:36:29'),(86,1,'Delete user : superadmin','127.0.0.1','http://localhost:8000/auth/user/22','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 09:38:28','2024-03-03 09:38:28'),(87,1,'Activate user : superadmin','127.0.0.1','http://localhost:8000/auth/user/22','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 09:38:57','2024-03-03 09:38:57'),(88,1,'Delete user : superadmin','127.0.0.1','http://localhost:8000/auth/user/22','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 09:39:42','2024-03-03 09:39:42'),(89,1,'Activate user : superadmin','127.0.0.1','http://localhost:8000/auth/user/22','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 09:39:52','2024-03-03 09:39:52'),(90,1,'Create user admin_comp_a','127.0.0.1','http://localhost:8000/auth/user','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 09:51:34','2024-03-03 09:51:34'),(91,1,'Create user admin_comp_bc','127.0.0.1','http://localhost:8000/auth/user','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 09:52:23','2024-03-03 09:52:23'),(92,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Safari','16.6','iOS','16_6','iPhone','16_6','2024-03-03 09:59:11','2024-03-03 09:59:11'),(93,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 10:08:59','2024-03-03 10:08:59'),(94,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 10:31:44','2024-03-03 10:31:44'),(95,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 10:37:52','2024-03-03 10:37:52'),(96,1,'Disable Company : Company B','127.0.0.1','http://localhost:8000/auth/company/34','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 10:40:33','2024-03-03 10:40:33'),(97,1,'Disable Company : Company C','127.0.0.1','http://localhost:8000/auth/company/35','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 10:44:26','2024-03-03 10:44:26'),(98,1,'Activate Company : Company B','127.0.0.1','http://localhost:8000/auth/company/34','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 10:45:42','2024-03-03 10:45:42'),(99,1,'Activate Company : Company C','127.0.0.1','http://localhost:8000/auth/company/35','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 10:45:45','2024-03-03 10:45:45'),(100,1,'Disable Company : Company A','127.0.0.1','http://localhost:8000/auth/company/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 10:46:09','2024-03-03 10:46:09'),(101,1,'Activate Company : Company A','127.0.0.1','http://localhost:8000/auth/company/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 10:46:34','2024-03-03 10:46:34'),(102,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 10:47:14','2024-03-03 10:47:14'),(103,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 13:01:35','2024-03-03 13:01:35'),(104,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 13:48:43','2024-03-03 13:48:43'),(105,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 13:49:05','2024-03-03 13:49:05'),(106,24,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 13:50:29','2024-03-03 13:50:29'),(107,24,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 14:00:08','2024-03-03 14:00:08'),(108,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 14:00:12','2024-03-03 14:00:12'),(109,1,'Force login : admin','127.0.0.1','http://localhost:8000/auth/force-login/24','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 14:02:21','2024-03-03 14:02:21'),(110,24,'Exit from login As : superadmin','127.0.0.1','http://localhost:8000/auth/back-login/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 14:03:21','2024-03-03 14:03:21'),(111,1,'Force login : admin','127.0.0.1','http://localhost:8000/auth/force-login/23','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 14:03:30','2024-03-03 14:03:30'),(112,23,'Exit from login As : superadmin','127.0.0.1','http://localhost:8000/auth/back-login/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 14:04:35','2024-03-03 14:04:35'),(113,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 14:04:40','2024-03-03 14:04:40'),(114,24,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 14:04:52','2024-03-03 14:04:52'),(115,24,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 15:19:33','2024-03-03 15:19:33'),(116,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 15:19:37','2024-03-03 15:19:37'),(117,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 15:20:25','2024-03-03 15:20:25'),(118,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 15:20:38','2024-03-03 15:20:38'),(119,24,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 15:20:44','2024-03-03 15:20:44'),(120,1,'Create Room : Room B','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 16:04:22','2024-03-03 16:04:22'),(121,1,'Create Room : Room 1, Room 1','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 16:28:50','2024-03-03 16:28:50'),(122,1,'Create Room : Room 1','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 16:33:06','2024-03-03 16:33:06'),(123,1,'Create Room : Room 1','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 16:33:24','2024-03-03 16:33:24'),(124,1,'Create Room : Room 2','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 16:34:03','2024-03-03 16:34:03'),(125,1,'Create Room : Room 1, Room 2, Room 1','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 16:53:29','2024-03-03 16:53:29'),(126,1,'Create Room : Room 1, Room 2, Room 1, Room 1, Room 3, Room 2','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:00:10','2024-03-03 17:00:10'),(127,1,'Create Room : Room 4','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:00:45','2024-03-03 17:00:45'),(128,1,'Update room name Room 1 to Room 1','127.0.0.1','http://localhost:8000/auth/room/30','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:41:58','2024-03-03 17:41:58'),(129,1,'Update room name Room 1 to Room 1','127.0.0.1','http://localhost:8000/auth/room/29','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:45:36','2024-03-03 17:45:36'),(130,1,'Update room name Room 1 to Room 1','127.0.0.1','http://localhost:8000/auth/room/29','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:47:48','2024-03-03 17:47:48'),(131,1,'Update room name Room 1 to Room 2','127.0.0.1','http://localhost:8000/auth/room/29','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:48:08','2024-03-03 17:48:08'),(132,1,'Disable Room : Room 1','127.0.0.1','http://localhost:8000/auth/room/30','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:54:39','2024-03-03 17:54:39'),(133,1,'Activate Room : Room 1','127.0.0.1','http://localhost:8000/auth/room/30','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:55:50','2024-03-03 17:55:50'),(134,1,'Disable Room : Room 1','127.0.0.1','http://localhost:8000/auth/room/30','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:56:02','2024-03-03 17:56:02'),(135,1,'Activate Room : Room 1','127.0.0.1','http://localhost:8000/auth/room/30','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:56:30','2024-03-03 17:56:30'),(136,1,'Create Room : Room 1','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:57:35','2024-03-03 17:57:35'),(137,1,'Create Room : Room 1, Room 1','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:58:23','2024-03-03 17:58:23'),(138,1,'Create Room : dfgdfg, sfsf','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:59:10','2024-03-03 17:59:10'),(139,1,'Create Room : Room 1, jhgf','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 17:59:49','2024-03-03 17:59:49'),(140,1,'Create Room : Room 1, Room 2, Room 1, Room 1','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 18:04:26','2024-03-03 18:04:26'),(141,24,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 18:05:05','2024-03-03 18:05:05'),(142,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-03 18:05:12','2024-03-03 18:05:12'),(143,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 03:15:59','2024-03-04 03:15:59'),(144,1,'Create Group : History','127.0.0.1','http://localhost:8000/auth/group','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 03:36:32','2024-03-04 03:36:32'),(145,1,'Create Group : Group 1, Group 2, Group 3','127.0.0.1','http://localhost:8000/auth/group','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 03:36:59','2024-03-04 03:36:59'),(146,1,'Update group name Group 3 to Sports','127.0.0.1','http://localhost:8000/auth/group/6','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 03:41:19','2024-03-04 03:41:19'),(147,1,'Disable Group : Group 1','127.0.0.1','http://localhost:8000/auth/group/4','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 03:47:27','2024-03-04 03:47:27'),(148,1,'Disable Group : Group 2','127.0.0.1','http://localhost:8000/auth/group/5','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 03:47:30','2024-03-04 03:47:30'),(149,1,'Activate Group : Group 1','127.0.0.1','http://localhost:8000/auth/group/4','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 03:47:57','2024-03-04 03:47:57'),(150,1,'Activate Group : Group 2','127.0.0.1','http://localhost:8000/auth/group/5','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 03:48:01','2024-03-04 03:48:01'),(151,1,'Create Exam Type : Exam Type 1','127.0.0.1','http://localhost:8000/auth/examtype','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 04:14:45','2024-03-04 04:14:45'),(152,1,'Create Exam Type : Exam Type 2, Exam Type 3','127.0.0.1','http://localhost:8000/auth/examtype','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 04:15:15','2024-03-04 04:15:15'),(153,1,'Update exam type name Exam Type 1 to Exam Type 1','127.0.0.1','http://localhost:8000/auth/examtype/3','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 04:19:44','2024-03-04 04:19:44'),(154,1,'Update exam type name Exam Type 2 to Exam Type Edit','127.0.0.1','http://localhost:8000/auth/examtype/4','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 04:20:05','2024-03-04 04:20:05'),(155,1,'Disable Exam Type : Exam Type Edit','127.0.0.1','http://localhost:8000/auth/examtype/4','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 04:22:20','2024-03-04 04:22:20'),(156,1,'Disable Exam Type : Exam Type 3','127.0.0.1','http://localhost:8000/auth/examtype/5','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 04:22:24','2024-03-04 04:22:24'),(157,1,'Activate Exam Type : Exam Type 3','127.0.0.1','http://localhost:8000/auth/examtype/5','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 04:22:54','2024-03-04 04:22:54'),(158,1,'Activate Exam Type : Exam Type Edit','127.0.0.1','http://localhost:8000/auth/examtype/4','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 04:22:57','2024-03-04 04:22:57'),(159,1,'Disable Group : Art','127.0.0.1','http://localhost:8000/auth/group/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 05:05:14','2024-03-04 05:05:14'),(160,1,'Activate Group : Art','127.0.0.1','http://localhost:8000/auth/group/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 05:07:55','2024-03-04 05:07:55'),(161,1,'Create Teacher : Teacher 1','127.0.0.1','http://localhost:8000/auth/teacher','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 06:10:43','2024-03-04 06:10:43'),(162,1,'Create Teacher : Teacher 1, Teacher 3, Teacher 4','127.0.0.1','http://localhost:8000/auth/teacher','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 06:11:54','2024-03-04 06:11:54'),(163,1,'Create Teacher : Teacher 1, Teacher 2, Teacher 3','127.0.0.1','http://localhost:8000/auth/teacher','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 06:13:18','2024-03-04 06:13:18'),(164,1,'Update teacher name Teacher 3 to Teacher Science','127.0.0.1','http://localhost:8000/auth/teacher/11','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 06:27:14','2024-03-04 06:27:14'),(165,1,'Update teacher name Teacher Science to Teacher Science','127.0.0.1','http://localhost:8000/auth/teacher/11','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 06:27:32','2024-03-04 06:27:32'),(166,1,'Disable Teacher : Teacher Science','127.0.0.1','http://localhost:8000/auth/teacher/11','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 06:29:50','2024-03-04 06:29:50'),(167,1,'Activate Teacher : Teacher Science','127.0.0.1','http://localhost:8000/auth/teacher/11','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 06:30:10','2024-03-04 06:30:10'),(168,1,'Create Teacher : Teacher Science','127.0.0.1','http://localhost:8000/auth/teacher','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 07:17:45','2024-03-04 07:17:45'),(169,1,'Create Exam : Exam 1','127.0.0.1','http://localhost:8000/auth/exam','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 09:03:40','2024-03-04 09:03:40'),(170,1,'Create Exam : Exam 1, Exam 3, Exam 2, Exam 2','127.0.0.1','http://localhost:8000/auth/exam','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 09:06:22','2024-03-04 09:06:22'),(171,1,'Create Exam : Mathematics','127.0.0.1','http://localhost:8000/auth/exam','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 09:22:37','2024-03-04 09:22:37'),(172,1,'Update exam name Mathematics to Mathematics','127.0.0.1','http://localhost:8000/auth/exam/9','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 09:26:30','2024-03-04 09:26:30'),(173,1,'Update exam name Mathematics to Language','127.0.0.1','http://localhost:8000/auth/exam/9','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 09:28:49','2024-03-04 09:28:49'),(174,1,'Disable Exam : Language','127.0.0.1','http://localhost:8000/auth/exam/9','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 09:31:38','2024-03-04 09:31:38'),(175,1,'Activate Exam : Language','127.0.0.1','http://localhost:8000/auth/exam/9','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 09:32:03','2024-03-04 09:32:03'),(176,1,'Create permission : examschedule','127.0.0.1','http://localhost:8000/auth/permission','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 09:42:10','2024-03-04 09:42:10'),(177,1,'Create permission : surveillance','127.0.0.1','http://localhost:8000/auth/permission','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 09:42:39','2024-03-04 09:42:39'),(178,1,'Edit Role : superadmin','127.0.0.1','http://localhost:8000/auth/role/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 09:43:07','2024-03-04 09:43:07'),(179,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 10:00:37','2024-03-04 10:00:37'),(180,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 13:39:27','2024-03-04 13:39:27'),(181,24,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 15:49:34','2024-03-04 15:49:34'),(182,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 15:50:11','2024-03-04 15:50:11'),(183,24,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 15:50:22','2024-03-04 15:50:22'),(184,24,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 15:50:27','2024-03-04 15:50:27'),(185,24,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 15:51:46','2024-03-04 15:51:46'),(186,23,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 15:51:53','2024-03-04 15:51:53'),(187,23,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 15:57:29','2024-03-04 15:57:29'),(188,24,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 15:57:42','2024-03-04 15:57:42'),(189,24,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 15:58:23','2024-03-04 15:58:23'),(190,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 17:06:03','2024-03-04 17:06:03'),(191,23,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 17:06:22','2024-03-04 17:06:22'),(192,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-04 17:06:53','2024-03-04 17:06:53'),(193,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 02:59:01','2024-03-05 02:59:01'),(194,1,'Create Company : Company Cuy','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 03:31:03','2024-03-05 03:31:03'),(195,1,'Create Company : Company Awe, Company Uye','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 03:31:40','2024-03-05 03:31:40'),(196,1,'Create Company : Awe, Weww','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 03:44:21','2024-03-05 03:44:21'),(197,1,'Update company name Awe to Awe','127.0.0.1','http://localhost:8000/auth/company/41','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 03:46:51','2024-03-05 03:46:51'),(198,1,'Update company name Awe to Awer','127.0.0.1','http://localhost:8000/auth/company/41','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 03:47:15','2024-03-05 03:47:15'),(199,1,'Disable Company : Weww','127.0.0.1','http://localhost:8000/auth/company/42','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 03:48:36','2024-03-05 03:48:36'),(200,1,'Activate Company : Weww','127.0.0.1','http://localhost:8000/auth/company/42','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 03:48:43','2024-03-05 03:48:43'),(201,1,'Disable Company : Weww','127.0.0.1','http://localhost:8000/auth/company/42','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 03:49:07','2024-03-05 03:49:07'),(202,1,'Activate Company : Weww','127.0.0.1','http://localhost:8000/auth/company/42','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 03:49:25','2024-03-05 03:49:25'),(203,1,'Create Room : Company F, Company 1','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 04:09:00','2024-03-05 04:09:00'),(204,1,'Create Room : asdasd','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 04:18:04','2024-03-05 04:18:04'),(205,1,'Create Room : Company F','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 05:46:42','2024-03-05 05:46:42'),(206,1,'Create Room : Company F, Company F','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 05:49:17','2024-03-05 05:49:17'),(207,1,'Update room name Company F to Company F1','127.0.0.1','http://localhost:8000/auth/room/69','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 05:53:52','2024-03-05 05:53:52'),(208,1,'Disable Room : Company F1','127.0.0.1','http://localhost:8000/auth/room/69','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 05:54:04','2024-03-05 05:54:04'),(209,1,'Create Room : Room Awe1, Room Awe2','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 05:55:54','2024-03-05 05:55:54'),(210,1,'Update room name Room Awe1 to Room Awe1','127.0.0.1','http://localhost:8000/auth/room/70','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 05:56:12','2024-03-05 05:56:12'),(211,23,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:00:10','2024-03-05 06:00:10'),(212,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:01:30','2024-03-05 06:01:30'),(213,23,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:02:11','2024-03-05 06:02:11'),(214,23,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:02:20','2024-03-05 06:02:20'),(215,24,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:03:18','2024-03-05 06:03:18'),(216,24,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:04:49','2024-03-05 06:04:49'),(217,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:05:03','2024-03-05 06:05:03'),(218,23,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:05:21','2024-03-05 06:05:21'),(219,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:05:44','2024-03-05 06:05:44'),(220,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:06:19','2024-03-05 06:06:19'),(221,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:06:50','2024-03-05 06:06:50'),(222,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:11:12','2024-03-05 06:11:12'),(223,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:14:03','2024-03-05 06:14:03'),(224,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:19:19','2024-03-05 06:19:19'),(225,1,'Update company name Company A to Company A','127.0.0.1','http://localhost:8000/auth/company/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:20:19','2024-03-05 06:20:19'),(226,1,'Update company name Company B to Company B','127.0.0.1','http://localhost:8000/auth/company/34','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:20:27','2024-03-05 06:20:27'),(227,1,'Update company name Company C to Company C','127.0.0.1','http://localhost:8000/auth/company/35','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:20:34','2024-03-05 06:20:34'),(228,23,'Create Room : Room A101, Room A102','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:21:54','2024-03-05 06:21:54'),(229,1,'Create Room : Room B101','127.0.0.1','http://localhost:8000/auth/room','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:22:39','2024-03-05 06:22:39'),(230,1,'Create Group : Mathematic, Art, English','127.0.0.1','http://localhost:8000/auth/group','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:30:53','2024-03-05 06:30:53'),(231,1,'Update group name English to Indonesia','127.0.0.1','http://localhost:8000/auth/group/9','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:31:23','2024-03-05 06:31:23'),(232,1,'Update group name Indonesia to Indonesia','127.0.0.1','http://localhost:8000/auth/group/9','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:31:30','2024-03-05 06:31:30'),(233,1,'Update group name Indonesia to English','127.0.0.1','http://localhost:8000/auth/group/9','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:31:40','2024-03-05 06:31:40'),(234,1,'Disable Group : English','127.0.0.1','http://localhost:8000/auth/group/9','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:31:46','2024-03-05 06:31:46'),(235,1,'Activate Group : English','127.0.0.1','http://localhost:8000/auth/group/9','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:31:50','2024-03-05 06:31:50'),(236,1,'Create Exam Type : National Exam, Measurement Test, Final Cycle Exam','127.0.0.1','http://localhost:8000/auth/examtype','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:44:01','2024-03-05 06:44:01'),(237,1,'Create Teacher : Eka Bagus Fernadi, Fernadi Bagus Eka, Uzumaki Bayu','127.0.0.1','http://localhost:8000/auth/teacher','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 06:46:09','2024-03-05 06:46:09'),(238,1,'Create Exam : National Exam A, Measurement Test A, National Exam A','127.0.0.1','http://localhost:8000/auth/exam','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 07:01:46','2024-03-05 07:01:46'),(239,1,'Update exam name National Exam A to National Exam A','127.0.0.1','http://localhost:8000/auth/exam/12','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 07:02:22','2024-03-05 07:02:22'),(240,1,'Update exam name National Exam A to National Exam A','127.0.0.1','http://localhost:8000/auth/exam/12','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 07:02:36','2024-03-05 07:02:36'),(241,1,'Update exam name National Exam A to National Exam B','127.0.0.1','http://localhost:8000/auth/exam/12','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 07:03:09','2024-03-05 07:03:09'),(242,1,'Update exam name National Exam B to National Exam B','127.0.0.1','http://localhost:8000/auth/exam/12','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 07:03:31','2024-03-05 07:03:31'),(243,1,'Create Company : Company D','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 08:17:20','2024-03-05 08:17:20'),(244,1,'Create Company : Company E','127.0.0.1','http://localhost:8000/auth/company','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 08:36:51','2024-03-05 08:36:51'),(245,1,'Update company name Company D to Company D','127.0.0.1','http://localhost:8000/auth/company/43','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 08:50:46','2024-03-05 08:50:46'),(246,1,'Update company name Company D to Company D','127.0.0.1','http://localhost:8000/auth/company/43','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 08:52:34','2024-03-05 08:52:34'),(247,1,'Update company name Company D to Company Z','127.0.0.1','http://localhost:8000/auth/company/43','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 08:52:56','2024-03-05 08:52:56'),(248,1,'Create Exam Schedule : 11, 10, 12','127.0.0.1','http://localhost:8000/auth/examschedule','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 09:23:08','2024-03-05 09:23:08'),(249,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 10:06:36','2024-03-05 10:06:36'),(250,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 14:54:41','2024-03-05 14:54:41'),(251,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 14:55:07','2024-03-05 14:55:07'),(252,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 14:55:14','2024-03-05 14:55:14'),(253,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 14:55:40','2024-03-05 14:55:40'),(254,1,'Update examschedule','127.0.0.1','http://localhost:8000/auth/examschedule/4','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 15:17:55','2024-03-05 15:17:55'),(255,1,'Update examschedule','127.0.0.1','http://localhost:8000/auth/examschedule/4','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 15:18:51','2024-03-05 15:18:51'),(256,1,'Disable Exam Schedule : National Exam B','127.0.0.1','http://localhost:8000/auth/examschedule/4','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 15:22:16','2024-03-05 15:22:16'),(257,1,'Activate Exam Schedule : National Exam B','127.0.0.1','http://localhost:8000/auth/examschedule/4','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 15:25:06','2024-03-05 15:25:06'),(258,1,'Update Exam Schedule : National Exam B','127.0.0.1','http://localhost:8000/auth/examschedule/4','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 15:25:30','2024-03-05 15:25:30'),(259,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-05 21:51:26','2024-03-05 21:51:26'),(260,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 00:20:11','2024-03-06 00:20:11'),(261,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 03:06:12','2024-03-06 03:06:12'),(262,1,'Create Surveillance : 7','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 09:12:50','2024-03-06 09:12:50'),(263,1,'Create Surveillance : 8','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 09:14:15','2024-03-06 09:14:15'),(264,1,'Create Surveillance : 9','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 09:14:48','2024-03-06 09:14:48'),(265,1,'Create Surveillance : 10','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 09:15:19','2024-03-06 09:15:19'),(266,1,'Create Surveillance : 11','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 09:16:23','2024-03-06 09:16:23'),(267,1,'Create Surveillance : 17','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 09:23:22','2024-03-06 09:23:22'),(268,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 10:08:29','2024-03-06 10:08:29'),(269,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 13:26:52','2024-03-06 13:26:52'),(270,1,'Delete Surveillance : 11','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 13:56:36','2024-03-06 13:56:36'),(271,1,'Delete Surveillance : 6','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 13:56:42','2024-03-06 13:56:42'),(272,1,'Delete Surveillance : 3','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 13:56:59','2024-03-06 13:56:59'),(273,1,'Delete Surveillance : 4','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 13:57:02','2024-03-06 13:57:02'),(274,1,'Create Surveillance : 24','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 13:58:14','2024-03-06 13:58:14'),(275,1,'Create Surveillance : 26','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 13:58:44','2024-03-06 13:58:44'),(276,1,'Create Surveillance : 27','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 13:59:03','2024-03-06 13:59:03'),(277,1,'Delete Surveillance : 27','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 13:59:18','2024-03-06 13:59:18'),(278,1,'Create Surveillance : 28','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 13:59:26','2024-03-06 13:59:26'),(279,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 14:13:42','2024-03-06 14:13:42'),(280,1,'Delete Surveillance : 28','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 14:13:58','2024-03-06 14:13:58'),(281,1,'Delete Surveillance : 26','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 14:14:00','2024-03-06 14:14:00'),(282,1,'Create Surveillance : 29','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 14:14:45','2024-03-06 14:14:45'),(283,1,'Create Surveillance : 30','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 14:14:53','2024-03-06 14:14:53'),(284,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 14:17:47','2024-03-06 14:17:47'),(285,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 14:26:46','2024-03-06 14:26:46'),(286,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 15:21:44','2024-03-06 15:21:44'),(287,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 15:32:46','2024-03-06 15:32:46'),(288,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-06 22:45:55','2024-03-06 22:45:55'),(289,23,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 05:18:25','2024-03-07 05:18:25'),(290,23,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 05:19:11','2024-03-07 05:19:11'),(291,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 05:24:29','2024-03-07 05:24:29'),(292,1,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 05:24:35','2024-03-07 05:24:35'),(293,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 05:25:35','2024-03-07 05:25:35'),(294,23,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 05:26:06','2024-03-07 05:26:06'),(295,1,'Edit Role : adminweb','127.0.0.1','http://localhost:8000/auth/role/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 05:27:23','2024-03-07 05:27:23'),(296,23,'Login','127.0.0.1','http://localhost:8000','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 05:27:37','2024-03-07 05:27:37'),(297,23,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 05:28:07','2024-03-07 05:28:07'),(298,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 06:36:57','2024-03-07 06:36:57'),(299,1,'Login','127.0.0.1','http://localhost:8000/login','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 06:37:14','2024-03-07 06:37:14'),(300,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 06:37:21','2024-03-07 06:37:21'),(301,1,'Login','127.0.0.1','http://localhost:8000/login','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 08:22:09','2024-03-07 08:22:09'),(302,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 08:22:16','2024-03-07 08:22:16'),(303,1,'Login','127.0.0.1','http://localhost:8000/login','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 09:11:35','2024-03-07 09:11:35'),(304,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 09:13:20','2024-03-07 09:13:20'),(305,1,'Login','127.0.0.1','http://localhost:8000/login','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 09:13:49','2024-03-07 09:13:49'),(306,1,'Create permission : Banner','127.0.0.1','http://localhost:8000/auth/permission','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 09:31:42','2024-03-07 09:31:42'),(307,1,'Create permission : about','127.0.0.1','http://localhost:8000/auth/permission','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 09:31:55','2024-03-07 09:31:55'),(308,1,'Edit Role : superadmin','127.0.0.1','http://localhost:8000/auth/role/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 09:32:07','2024-03-07 09:32:07'),(309,1,'Update about','127.0.0.1','http://localhost:8000/auth/about/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 09:56:30','2024-03-07 09:56:30'),(310,1,'Update about','127.0.0.1','http://localhost:8000/auth/about/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 09:57:13','2024-03-07 09:57:13'),(311,1,'Disable Banner : Slider 1','127.0.0.1','http://localhost:8000/auth/banner/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 10:20:44','2024-03-07 10:20:44'),(312,1,'Disable Banner : Slider 2','127.0.0.1','http://localhost:8000/auth/banner/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 10:21:34','2024-03-07 10:21:34'),(313,1,'Activate Banner : Slider 1','127.0.0.1','http://localhost:8000/auth/banner/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 10:21:44','2024-03-07 10:21:44'),(314,1,'Activate Banner : Slider 2','127.0.0.1','http://localhost:8000/auth/banner/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 10:21:47','2024-03-07 10:21:47'),(315,1,'Create Banner : Slider_3','127.0.0.1','http://localhost:8000/auth/banner','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 10:22:26','2024-03-07 10:22:26'),(316,1,'Update banner','127.0.0.1','http://localhost:8000/auth/banner/3','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 10:23:14','2024-03-07 10:23:14'),(317,1,'Update banner','127.0.0.1','http://localhost:8000/auth/banner/3','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 10:23:26','2024-03-07 10:23:26'),(318,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-07 10:27:14','2024-03-07 10:27:14'),(319,1,'Login','127.0.0.1','http://localhost:8000/login','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 09:42:46','2024-03-11 09:42:46'),(320,1,'Create Teacher : Windah Basudara','127.0.0.1','http://localhost:8000/auth/teacher','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 09:45:36','2024-03-11 09:45:36'),(321,1,'Create Surveillance : 31','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 09:47:00','2024-03-11 09:47:00'),(322,1,'Create Surveillance : 32','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 09:56:40','2024-03-11 09:56:40'),(323,1,'Create Surveillance : 33','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:00:24','2024-03-11 10:00:24'),(324,1,'Delete Surveillance : 29','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:09:18','2024-03-11 10:09:18'),(325,1,'Create Surveillance : 34','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:11:44','2024-03-11 10:11:44'),(326,1,'Delete Surveillance : 30','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:12:12','2024-03-11 10:12:12'),(327,1,'Delete Surveillance : 34','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:12:51','2024-03-11 10:12:51'),(328,1,'Delete Surveillance : 32','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:13:13','2024-03-11 10:13:13'),(329,1,'Create Surveillance : 35','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:13:25','2024-03-11 10:13:25'),(330,1,'Create Surveillance : 36','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:14:26','2024-03-11 10:14:26'),(331,1,'Create Surveillance : 37','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:14:52','2024-03-11 10:14:52'),(332,1,'Create Surveillance : 38','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:15:16','2024-03-11 10:15:16'),(333,1,'Delete Surveillance : 38','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:15:25','2024-03-11 10:15:25'),(334,1,'Create Surveillance : 40','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:15:58','2024-03-11 10:15:58'),(335,1,'Delete Surveillance : 31','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:21:44','2024-03-11 10:21:44'),(336,1,'Delete Surveillance : 37','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:21:47','2024-03-11 10:21:47'),(337,1,'Delete Surveillance : 36','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:21:49','2024-03-11 10:21:49'),(338,1,'Delete Surveillance : 40','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:22:19','2024-03-11 10:22:19'),(339,1,'Delete Surveillance : 35','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:22:21','2024-03-11 10:22:21'),(340,1,'Delete Surveillance : 33','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:22:23','2024-03-11 10:22:23'),(341,1,'Create Exam Schedule : 10','127.0.0.1','http://localhost:8000/auth/examschedule','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:23:33','2024-03-11 10:23:33'),(342,1,'Create Surveillance : 41','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:25:42','2024-03-11 10:25:42'),(343,1,'Create Surveillance : 42','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:25:52','2024-03-11 10:25:52'),(344,1,'Create Surveillance : 46','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 10:32:34','2024-03-11 10:32:34'),(345,1,'Create Surveillance : 47','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:06:30','2024-03-11 11:06:30'),(346,1,'Create Surveillance : 48','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:13:26','2024-03-11 11:13:26'),(347,1,'Delete Surveillance : 41','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:13:36','2024-03-11 11:13:36'),(348,1,'Create Surveillance : 49','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:14:09','2024-03-11 11:14:09'),(349,1,'Update Exam Schedule : National Exam A','127.0.0.1','http://localhost:8000/auth/examschedule/3','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:15:49','2024-03-11 11:15:49'),(350,1,'Update Exam Schedule : National Exam A','127.0.0.1','http://localhost:8000/auth/examschedule/3','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:16:26','2024-03-11 11:16:26'),(351,1,'Update Exam Schedule : National Exam A','127.0.0.1','http://localhost:8000/auth/examschedule/3','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:17:53','2024-03-11 11:17:53'),(352,1,'Disable Exam Schedule : National Exam A','127.0.0.1','http://localhost:8000/auth/examschedule/3','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:18:41','2024-03-11 11:18:41'),(353,1,'Activate Exam Schedule : National Exam A','127.0.0.1','http://localhost:8000/auth/examschedule/3','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:29:09','2024-03-11 11:29:09'),(354,1,'Delete Surveillance : 48','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:30:56','2024-03-11 11:30:56'),(355,1,'Delete Surveillance : 47','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:30:59','2024-03-11 11:30:59'),(356,1,'Delete Surveillance : 49','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:31:15','2024-03-11 11:31:15'),(357,1,'Delete Surveillance : 42','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:31:40','2024-03-11 11:31:40'),(358,1,'Create Surveillance : 50','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:31:57','2024-03-11 11:31:57'),(359,1,'Disable Exam Schedule : National Exam A','127.0.0.1','http://localhost:8000/auth/examschedule/3','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 11:37:37','2024-03-11 11:37:37'),(360,1,'Delete Surveillance : 24','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:08:35','2024-03-11 12:08:35'),(361,1,'Create Surveillance : 51','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:08:54','2024-03-11 12:08:54'),(362,1,'Activate Exam Schedule : National Exam A','127.0.0.1','http://localhost:8000/auth/examschedule/3','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:09:20','2024-03-11 12:09:20'),(363,1,'Delete Surveillance : 50','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:09:32','2024-03-11 12:09:32'),(364,1,'Create Surveillance : 52','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:09:39','2024-03-11 12:09:39'),(365,1,'Create Surveillance : 53','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:15:31','2024-03-11 12:15:31'),(366,1,'Disable Exam Schedule : National Exam A','127.0.0.1','http://localhost:8000/auth/examschedule/5','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:19:47','2024-03-11 12:19:47'),(367,1,'Activate Exam Schedule : National Exam A','127.0.0.1','http://localhost:8000/auth/examschedule/5','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:20:14','2024-03-11 12:20:14'),(368,1,'Create Surveillance : 54','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:20:38','2024-03-11 12:20:38'),(369,1,'Create Surveillance : 55','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:20:41','2024-03-11 12:20:41'),(370,1,'Create Surveillance : 56','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:20:44','2024-03-11 12:20:44'),(371,1,'Disable Exam Schedule : National Exam A','127.0.0.1','http://localhost:8000/auth/examschedule/5','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:21:24','2024-03-11 12:21:24'),(372,1,'Activate Exam Schedule : National Exam A','127.0.0.1','http://localhost:8000/auth/examschedule/5','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:22:32','2024-03-11 12:22:32'),(373,1,'Create Surveillance : 57','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:23:18','2024-03-11 12:23:18'),(374,1,'Create Surveillance : 58','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:23:21','2024-03-11 12:23:21'),(375,1,'Create Surveillance : 59','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:23:23','2024-03-11 12:23:23'),(376,1,'Delete Surveillance : 59','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:24:12','2024-03-11 12:24:12'),(377,1,'Delete Surveillance : 58','127.0.0.1','http://localhost:8000/auth/surveillance-delete','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:24:15','2024-03-11 12:24:15'),(378,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-11 12:25:58','2024-03-11 12:25:58'),(379,1,'Login','127.0.0.1','http://localhost:8000/login','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-13 02:05:03','2024-03-13 02:05:03'),(380,1,'Create Surveillance : 60','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-13 02:05:54','2024-03-13 02:05:54'),(381,1,'Create Surveillance : 61','127.0.0.1','http://localhost:8000/auth/surveillance-add','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-13 02:06:05','2024-03-13 02:06:05'),(382,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-13 02:16:16','2024-03-13 02:16:16'),(383,1,'Login','127.0.0.1','http://localhost:8000/login','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 05:02:08','2024-03-19 05:02:08'),(384,1,'Delete permission : exam','127.0.0.1','http://localhost:8000/auth/permission/171','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 05:04:22','2024-03-19 05:04:22'),(385,1,'Delete permission : examschedule','127.0.0.1','http://localhost:8000/auth/permission/176','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 05:04:25','2024-03-19 05:04:25'),(386,1,'Delete permission : examtype','127.0.0.1','http://localhost:8000/auth/permission/166','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 05:04:29','2024-03-19 05:04:29'),(387,1,'Delete permission : group','127.0.0.1','http://localhost:8000/auth/permission/151','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 05:04:33','2024-03-19 05:04:33'),(388,1,'Delete permission : room','127.0.0.1','http://localhost:8000/auth/permission/161','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 05:04:40','2024-03-19 05:04:40'),(389,1,'Delete permission : surveillance','127.0.0.1','http://localhost:8000/auth/permission/181','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 05:04:44','2024-03-19 05:04:44'),(390,1,'Delete permission : teacher','127.0.0.1','http://localhost:8000/auth/permission/156','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 05:04:50','2024-03-19 05:04:50'),(391,1,'Disable Company : Company Z','127.0.0.1','http://localhost:8000/auth/company/43','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 05:07:02','2024-03-19 05:07:02'),(392,1,'Disable Company : Company E','127.0.0.1','http://localhost:8000/auth/company/44','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 05:07:06','2024-03-19 05:07:06'),(393,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 06:10:00','2024-03-19 06:10:00'),(394,1,'Login','127.0.0.1','http://localhost:8000/login','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 06:11:31','2024-03-19 06:11:31'),(395,1,'Create permission : product','127.0.0.1','http://localhost:8000/auth/permission','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 07:02:14','2024-03-19 07:02:14'),(396,1,'Edit Role : superadmin','127.0.0.1','http://localhost:8000/auth/role/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 07:02:28','2024-03-19 07:02:28'),(397,1,'Create Product : ffff','127.0.0.1','http://localhost:8000/auth/product','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 07:19:56','2024-03-19 07:19:56'),(398,1,'Disable product : edit_produk1','127.0.0.1','http://localhost:8000/auth/product/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 07:22:56','2024-03-19 07:22:56'),(399,1,'Activate product : edit_produk1','127.0.0.1','http://localhost:8000/auth/product/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 07:23:01','2024-03-19 07:23:01'),(400,1,'Update product name edit_produk1 to edit_produk1','127.0.0.1','http://localhost:8000/auth/product/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 07:24:47','2024-03-19 07:24:47'),(401,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 07:30:10','2024-03-19 07:30:10'),(402,1,'Login','127.0.0.1','http://localhost:8000/login','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 07:31:32','2024-03-19 07:31:32'),(403,1,'Update banner','127.0.0.1','http://localhost:8000/auth/banner/1','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 07:34:09','2024-03-19 07:34:09'),(404,1,'Update banner','127.0.0.1','http://localhost:8000/auth/banner/2','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 07:34:25','2024-03-19 07:34:25'),(405,1,'Logout','127.0.0.1','http://localhost:8000/auth/logout','Chrome','122.0.0.0','Windows','10.0','WebKit','0','2024-03-19 08:20:14','2024-03-19 08:20:14');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2024_03_17_144806_create_users_table',1),(2,'2024_03_18_140958_create_products_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_role` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (111,1),(112,1),(113,1),(114,1),(115,1),(116,1),(117,1),(118,1),(119,1),(120,1),(121,1),(122,1),(123,1),(124,1),(125,1),(126,1),(127,1),(128,1),(129,1),(130,1),(146,1),(147,1),(148,1),(149,1),(150,1),(186,1),(187,1),(188,1),(189,1),(190,1),(191,1),(192,1),(193,1),(194,1),(195,1),(196,1),(197,1),(198,1),(199,1),(200,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_user`
--

DROP TABLE IF EXISTS `permission_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_user` (
  `permission_id` bigint unsigned NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  `user_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  KEY `permission_user_permission_id_foreign` (`permission_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_user`
--

LOCK TABLES `permission_user` WRITE;
/*!40000 ALTER TABLE `permission_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `grup` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (111,'user','user-create',' create','Users for web','2024-02-21 09:14:54','2024-02-21 09:14:54'),(112,'user','user-read',' read','Users for web','2024-02-21 09:14:54','2024-02-21 09:14:54'),(113,'user','user-update',' update','Users for web','2024-02-21 09:14:54','2024-02-21 09:14:54'),(114,'user','user-delete',' delete','Users for web','2024-02-21 09:14:54','2024-02-21 09:14:54'),(115,'user','user-print',' print','Users for web','2024-02-21 09:14:54','2024-02-21 09:14:54'),(116,'umum','umum-create',' create','General setting for web','2024-02-21 09:16:38','2024-02-21 09:16:38'),(117,'umum','umum-read',' read','General setting for web','2024-02-21 09:16:38','2024-02-21 09:16:38'),(118,'umum','umum-update',' update','General setting for web','2024-02-21 09:16:38','2024-02-21 09:16:38'),(119,'umum','umum-delete',' delete','General setting for web','2024-02-21 09:16:38','2024-02-21 09:16:38'),(120,'umum','umum-print',' print','General setting for web','2024-02-21 09:16:38','2024-02-21 09:16:38'),(121,'role','role-create',' create','Role users','2024-02-21 09:17:00','2024-02-21 09:17:00'),(122,'role','role-read',' read','Role users','2024-02-21 09:17:00','2024-02-21 09:17:00'),(123,'role','role-update',' update','Role users','2024-02-21 09:17:00','2024-02-21 09:17:00'),(124,'role','role-delete',' delete','Role users','2024-02-21 09:17:00','2024-02-21 09:17:00'),(125,'role','role-print',' print','Role users','2024-02-21 09:17:00','2024-02-21 09:17:00'),(126,'permission','permission-create',' create','Permission access web','2024-02-21 09:17:16','2024-02-21 09:17:16'),(127,'permission','permission-read',' read','Permission access web','2024-02-21 09:17:16','2024-02-21 09:17:16'),(128,'permission','permission-update',' update','Permission access web','2024-02-21 09:17:16','2024-02-21 09:17:16'),(129,'permission','permission-delete',' delete','Permission access web','2024-02-21 09:17:16','2024-02-21 09:17:16'),(130,'permission','permission-print',' print','Permission access web','2024-02-21 09:17:16','2024-02-21 09:17:16'),(146,'company','company-create',' create','Permission to access company menu','2024-03-02 02:49:00','2024-03-02 02:49:00'),(147,'company','company-read',' read','Permission to access company menu','2024-03-02 02:49:00','2024-03-02 02:49:00'),(148,'company','company-update',' update','Permission to access company menu','2024-03-02 02:49:00','2024-03-02 02:49:00'),(149,'company','company-delete',' delete','Permission to access company menu','2024-03-02 02:49:00','2024-03-02 02:49:00'),(150,'company','company-print',' print','Permission to access company menu','2024-03-02 02:49:00','2024-03-02 02:49:00'),(186,'banner','banner-create',' create','Permission to access banner menu','2024-03-07 09:31:42','2024-03-07 09:31:42'),(187,'banner','banner-read',' read','Permission to access banner menu','2024-03-07 09:31:42','2024-03-07 09:31:42'),(188,'banner','banner-update',' update','Permission to access banner menu','2024-03-07 09:31:42','2024-03-07 09:31:42'),(189,'banner','banner-delete',' delete','Permission to access banner menu','2024-03-07 09:31:42','2024-03-07 09:31:42'),(190,'banner','banner-print',' print','Permission to access banner menu','2024-03-07 09:31:42','2024-03-07 09:31:42'),(191,'about','about-create',' create','Permission to access about menu','2024-03-07 09:31:55','2024-03-07 09:31:55'),(192,'about','about-read',' read','Permission to access about menu','2024-03-07 09:31:55','2024-03-07 09:31:55'),(193,'about','about-update',' update','Permission to access about menu','2024-03-07 09:31:55','2024-03-07 09:31:55'),(194,'about','about-delete',' delete','Permission to access about menu','2024-03-07 09:31:55','2024-03-07 09:31:55'),(195,'about','about-print',' print','Permission to access about menu','2024-03-07 09:31:55','2024-03-07 09:31:55'),(196,'product','product-create',' create','Permission to access product menu','2024-03-19 07:02:14','2024-03-19 07:02:14'),(197,'product','product-read',' read','Permission to access product menu','2024-03-19 07:02:14','2024-03-19 07:02:14'),(198,'product','product-update',' update','Permission to access product menu','2024-03-19 07:02:14','2024-03-19 07:02:14'),(199,'product','product-delete',' delete','Permission to access product menu','2024-03-19 07:02:14','2024-03-19 07:02:14'),(200,'product','product-print',' print','Permission to access product menu','2024-03-19 07:02:14','2024-03-19 07:02:14');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `photo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint unsigned NOT NULL DEFAULT '0',
  `status` enum('y','n') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'1710833084.png','edit_produk1',5000,'y','2024-03-19 03:29:14','2024-03-19 03:55:50'),(2,'1710832792.jpg','ad',10000,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(3,'1710832792.jpg','ex',4000,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(4,'1710832792.jpg','voluptatem',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(5,'1710832792.jpg','laudantium',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(6,'1710832792.jpg','ut',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(7,'1710832792.jpg','saepe',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(8,'1710832792.jpg','et',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(9,'1710832792.jpg','deleniti',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(10,'1710832792.jpg','nihil',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(11,'1710832792.jpg','ut',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(12,'1710832792.jpg','consectetur',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(13,'1710832792.jpg','doloremque',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(14,'1710832792.jpg','vel',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(15,'1710832792.jpg','dolor',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(16,'1710832792.jpg','dolor',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(17,'1710832792.jpg','omnis',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(18,'1710832792.jpg','voluptatum',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(19,'1710832792.jpg','modi',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(20,'1710832792.jpg','nisi',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(21,'1710832792.jpg','ullam',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(22,'1710832792.jpg','quia',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(23,'1710832792.jpg','hic',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(24,'1710832792.jpg','totam',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(25,'1710832792.jpg','ut',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(26,'1710832792.jpg','ex',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(27,'1710832792.jpg','illum',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(28,'1710832792.jpg','amet',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(29,'1710832792.jpg','aliquam',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(30,'1710832792.jpg','quis',0,'y','2024-03-19 03:29:14','2024-03-19 03:29:14'),(31,'1710832792.jpg','produk1',0,'n','2024-03-19 03:50:33','2024-03-19 03:57:09'),(32,'1710832792.jpg','ffff',12000,'y',NULL,NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_user` (
  `role_id` bigint unsigned NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  `user_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1,'App\\Models\\User'),(2,2,'App\\Models\\User'),(2,23,'App\\Models\\User'),(2,24,'App\\Models\\User');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'superadmin','Superadmin','All access','2023-04-13 01:28:29','2024-02-21 09:10:44'),(2,'adminweb','adminweb','All access except setting','2023-05-11 09:43:36','2024-02-21 09:10:21');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `umums`
--

DROP TABLE IF EXISTS `umums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `umums` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `favicon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `umums`
--

LOCK TABLES `umums` WRITE;
/*!40000 ALTER TABLE `umums` DISABLE KEYS */;
INSERT INTO `umums` VALUES (1,'Fernadi','1708499421.png','1708499431.png','2023-04-13 01:28:29','2024-02-21 07:10:34');
/*!40000 ALTER TABLE `umums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_company`
--

DROP TABLE IF EXISTS `user_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_company` (
  `user_id` int NOT NULL,
  `company_id` int unsigned NOT NULL,
  `user_input` int NOT NULL,
  `time_input` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `user_company_unique` (`user_id`,`company_id`),
  KEY `user_company_companies_FK` (`company_id`),
  KEY `user_company_users_FK_1` (`user_input`),
  CONSTRAINT `user_company_companies_FK` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_company_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_company_users_FK_1` FOREIGN KEY (`user_input`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_company`
--

LOCK TABLES `user_company` WRITE;
/*!40000 ALTER TABLE `user_company` DISABLE KEYS */;
INSERT INTO `user_company` VALUES (23,1,1,'2024-03-03 16:51:34'),(24,34,1,'2024-03-03 16:52:23'),(24,35,1,'2024-03-03 16:52:23');
/*!40000 ALTER TABLE `user_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `username` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `user_telpon` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `email` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `status` enum('y','n') CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT 'y',
  `foto` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci,
  `last_update` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int DEFAULT NULL COMMENT 'user id',
  `userinput` int DEFAULT NULL,
  `time_input` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timezone` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `ip` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'superadmin','admin','123456','admin@gmail.com','$2y$10$upBxiFKXI/zXEtBMG3HIMOGpD5wymbU1/C1qOFVuWTHjpzjxjDsIC',NULL,NULL,'y',NULL,'2024-02-21 23:32:18',NULL,NULL,'2023-04-13 08:28:29',NULL,NULL),(2,'adminweb','adminweb','0895632349518','adminweb@gmail.com','$2y$10$5mDs1X3FLMBtkbImnaG3eeSWm9mBdBT3wf.lYKsKqjIhpqmjHO13u',NULL,NULL,'y','','2024-03-03 16:20:27',NULL,NULL,'2023-05-11 16:43:00',NULL,NULL),(23,'Admin Company A','admin_comp_a','089123123123','admincompa@gmail.com','$2y$10$qhxW3oJAgjmLCF/8dDWiBOUdyDNH7WFdALlFMVhRS3jAztURJ2dH.',NULL,NULL,'y','',NULL,NULL,NULL,'2024-03-03 16:51:34',NULL,NULL),(24,'Admin Company BC','admin_comp_bc','089321321321','admincompbc@gmail.com','$2y$10$jLUBRZ5Yk9TNeOQ/oiz/K.bj8DGvp83n8qV3UWtyzb9FsZMtdQUDS',NULL,NULL,'y','',NULL,NULL,NULL,'2024-03-03 16:52:23',NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'eka_api'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-19 15:22:00
