/**
 * Main
 */

'use strict';

let menu, animate;

(function () {
    // Initialize menu
    //-----------------

    let layoutMenuEl = document.querySelectorAll('#layout-menu');
    layoutMenuEl.forEach(function (element) {
        menu = new Menu(element, {
            orientation: 'vertical',
            closeChildren: false
        });
        // Change parameter to true if you want scroll animation
        window.Helpers.scrollToActive((animate = false));
        window.Helpers.mainMenu = menu;
    });

    // Initialize menu togglers and bind click on each
    let menuToggler = document.querySelectorAll('.layout-menu-toggle');
    menuToggler.forEach(item => {
        item.addEventListener('click', event => {
            event.preventDefault();
            window.Helpers.toggleCollapsed();
        });
    });

    // Display menu toggle (layout-menu-toggle) on hover with delay
    let delay = function (elem, callback) {
        let timeout = null;
        elem.onmouseenter = function () {
            // Set timeout to be a timer which will invoke callback after 300ms (not for small screen)
            if (!Helpers.isSmallScreen()) {
                timeout = setTimeout(callback, 300);
            } else {
                timeout = setTimeout(callback, 0);
            }
        };

        elem.onmouseleave = function () {
            // Clear any timers set to timeout
            document.querySelector('.layout-menu-toggle').classList.remove('d-block');
            clearTimeout(timeout);
        };
    };
    if (document.getElementById('layout-menu')) {
        delay(document.getElementById('layout-menu'), function () {
            // not for small screen
            if (!Helpers.isSmallScreen()) {
                document.querySelector('.layout-menu-toggle').classList.add('d-block');
            }
        });
    }

    // Display in main menu when menu scrolls
    let menuInnerContainer = document.getElementsByClassName('menu-inner'),
        menuInnerShadow = document.getElementsByClassName('menu-inner-shadow')[0];
    if (menuInnerContainer.length > 0 && menuInnerShadow) {
        menuInnerContainer[0].addEventListener('ps-scroll-y', function () {
            if (this.querySelector('.ps__thumb-y').offsetTop) {
                menuInnerShadow.style.display = 'block';
            } else {
                menuInnerShadow.style.display = 'none';
            }
        });
    }

    // Init helpers & misc
    // --------------------

    // Init BS Tooltip
    const tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
    tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl);
    });

    // Accordion active class
    const accordionActiveFunction = function (e) {
        if (e.type == 'show.bs.collapse' || e.type == 'show.bs.collapse') {
            e.target.closest('.accordion-item').classList.add('active');
        } else {
            e.target.closest('.accordion-item').classList.remove('active');
        }
    };

    const accordionTriggerList = [].slice.call(document.querySelectorAll('.accordion'));
    const accordionList = accordionTriggerList.map(function (accordionTriggerEl) {
        accordionTriggerEl.addEventListener('show.bs.collapse', accordionActiveFunction);
        accordionTriggerEl.addEventListener('hide.bs.collapse', accordionActiveFunction);
    });

    // Auto update layout based on screen size
    window.Helpers.setAutoUpdate(true);

    // Toggle Password Visibility
    window.Helpers.initPasswordToggle();

    // Speech To Text
    window.Helpers.initSpeechToText();

    // Manage menu expanded/collapsed with templateCustomizer & local storage
    //------------------------------------------------------------------

    // If current layout is horizontal OR current window screen is small (overlay menu) than return from here
    if (window.Helpers.isSmallScreen()) {
        return;
    }

    // If current layout is vertical and current window screen is > small

    // Auto update menu collapsed/expanded based on the themeConfig
    window.Helpers.setCollapsed(true, false);
})();


// ========================= CUSTOM =================================
$(document).ready(function () {
    var token = $('input[name="_token"]').val();

    $('.select2').select2();

    // datatables checkbox
    $("#checkAll").click(function () {
        $('.dt-checkboxes').not(this).prop('checked', this.checked);
    });

    // ================================= ROLE SELECT
    $('.role-select').each(function () {
        let url = $(this).data('url');

        $('.role-select').select2({
            placeholder: " - Role -",
            allowClear: true,
            ajax: {
                url: url,
                type: 'POST',
                dataType: 'json',
                data: function (params) {
                    return {
                        term: params.term,
                        _token: token,
                    }
                },
                processResults: function (data) {
                    const status = data.status;

                    if (status == true) {
                        const record = data.data;

                        return {
                            results: $.map(record, function (item) {
                                return {
                                    id: item.id,
                                    text: item.label,
                                }
                            }),
                        };
                    }
                }
            }
        });
    });

    $('.company-select-combo').select2({
        placeholder: " - Company -",
        allowClear: true,
        ajax: {
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    term: params.term,
                    _token: token,
                }
            },
            processResults: function (data) {
                const status = data.status;

                if (status == true) {
                    const record = data.data;
                    return {
                        results: $.map(record, function (item) {
                            return {
                                id: item.id,
                                text: item.label,
                            }
                        }),
                    };
                }
            }
        }
    }).on('select2:select', function (e) {
        const target = e.currentTarget;
        const data_id = $(target).data('id');
        const exam_url = $(target).data('exam-url');

        $.ajax({
            type: 'POST',
            url: exam_url,
            data: {
                company: $(`.company-${data_id}`).val(),
                _token: token,
            }
        })
            .done(function (data) {
                const data_exam = data.data;
                let option = '';
                $.each(data_exam, function (i, val) {
                    option += `<option value="${val.id}">${val.label}</option>`;
                });

                $(`.exam-${data_id}`).removeAttr('disabled').html(option);
            })
            .fail(function (err) {
                console.log(err);
            });

    }).on('select2:clear', function (e) {
        const target = e.currentTarget;
        const data_id = $(target).data('id');

        $(`.exam-${data_id}`).attr('disabled', 'disabled').html('');
    });
    
    $('.company-select').select2({
        placeholder: " - Company -",
        allowClear: true,
        ajax: {
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    term: params.term,
                    _token: token,
                }
            },
            processResults: function (data) {
                const status = data.status;
                if (status == true) {
                    const record = data.data;
                    return {
                        results: $.map(record, function (item) {
                            return {
                                id: item.id,
                                text: item.label,
                            }
                        }),
                    };
                }
            }
        }
    }).on('select2:select', function () {
        $('.exam-select').html('');
        $('.exam-select').removeAttr('disabled');
    }).on('select2:clear', function () {
        $('.exam-select').html('');
    });
    
    $('.group-select').select2({
        placeholder: " - Group -",
        allowClear: true,
        ajax: {
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    term: params.term,
                    _token: token,
                }
            },
            processResults: function (data) {
                const status = data.status;
                if (status == true) {
                    const record = data.data;
                    return {
                        results: $.map(record, function (item) {
                            return {
                                id: item.id,
                                text: item.label,
                            }
                        }),
                    };
                }
            }
        }
    });

    $('.examtype-select').select2({
        placeholder: " - Exam Type -",
        allowClear: true,
        ajax: {
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    term: params.term,
                    _token: token,
                }
            },
            processResults: function (data) {
                const status = data.status;
                if (status == true) {
                    const record = data.data;
                    return {
                        results: $.map(record, function (item) {
                            return {
                                id: item.id,
                                text: item.label,
                            }
                        }),
                    };
                }
            }
        }
    });

    $('.method-select').select2({
        placeholder: " - Method -",
        allowClear: true,
        ajax: {
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    term: params.term,
                    _token: token,
                }
            },
            processResults: function (data) {
                const status = data.status;
                if (status == true) {
                    const record = data.data;
                    return {
                        results: $.map(record, function (item) {
                            return {
                                id: item.id,
                                text: item.label,
                            }
                        }),
                    };
                }
            }
        }
    });

    $('.grade-select').select2({
        placeholder: " - Grade -",
        allowClear: true,
        ajax: {
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    term: params.term,
                    _token: token,
                }
            },
            processResults: function (data) {
                const status = data.status;
                if (status == true) {
                    const record = data.data;
                    return {
                        results: $.map(record, function (item) {
                            return {
                                id: item.id,
                                text: item.label,
                            }
                        }),
                    };
                }
            }
        }
    });

    $('.fase-select').select2({
        placeholder: " - Fase -",
        allowClear: true,
        ajax: {
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    term: params.term,
                    _token: token,
                }
            },
            processResults: function (data) {
                const status = data.status;
                if (status == true) {
                    const record = data.data;
                    return {
                        results: $.map(record, function (item) {
                            return {
                                id: item.id,
                                text: item.label,
                            }
                        }),
                    };
                }
            }
        }
    });

    $('.exam-select').select2({
        placeholder: " - Exam -",
        allowClear: true,
        ajax: {
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    term: params.term,
                    _token: token,
                    company: $(`.company-select`).val(),
                }
            },
            processResults: function (data) {
                const status = data.status;
                if (status == true) {
                    const record = data.data;
                    return {
                        results: $.map(record, function (item) {
                            return {
                                id: item.id,
                                text: item.label,
                            }
                        }),
                    };
                }
            }
        }
    });

    $('.room-select').select2({
        placeholder: " - Room -",
        allowClear: true,
        ajax: {
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    term: params.term,
                    _token: token,
                    company: $(`.company-insert`).val(),
                }
            },
            processResults: function (data) {
                const status = data.status;
                if (status == true) {
                    const record = data.data;
                    return {
                        results: $.map(record, function (item) {
                            return {
                                id: item.id,
                                text: item.label,
                            }
                        }),
                    };
                }
            }
        }
    });

    $('.surveillancetype-select').select2({
        placeholder: " - Surveillance Type -",
        allowClear: true,
        ajax: {
            type: 'POST',
            dataType: 'json',
            data: function (params) {
                return {
                    term: params.term,
                    _token: token,
                }
            },
            processResults: function (data) {
                const status = data.status;
                if (status == true) {
                    const record = data.data;
                    return {
                        results: $.map(record, function (item) {
                            return {
                                id: item.id,
                                text: item.label,
                            }
                        }),
                    };
                }
            }
        }
    });

    // export
    $(document).on('click', '.btn-export', function (e) {
        e.preventDefault();

        let exportButton = $('#exportButton');
        let dropdownMenu = exportButton.next('.dropdown-menu');
        dropdownMenu.dropdown('hide');
        exportButton.prop('disabled', true); // Disable the button
        exportButton.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...'); // Change the button text to indicate loading

        let url = $(this).attr('href');
        let ext = $(this).data('ext');
        let filename = $(this).data('filename');

        $.ajax({
            type: 'POST',
            xhrFields: {
                responseType: 'blob',
            },
            cache: false,
            url: url,
            data: {
                _token: $('input[name="_token"]').val(),
                ext: ext,
                export: 'export',
                cari: $('#cari').val(),
                tanggal: $('#tanggal').val(),
                tmulai: $('#tmulai').val(),
                takhir: $('#takhir').val(),
                status: $('.btn-check:checked').val(),
                jenis: $('#jenis').val(),
                region: $('#region').val(),
                area: $('#area').val(),
                jabatan: $('#jabatan').val(),
                kategori: $('#kategori').val(),
                oos: $('#oos').val(),
                manpower: $('#manpower').val(),
                outlet: $('#outlet').val(),
                account: $('#account').val(),
                produk: $('#produk').val(),
                alokasi: $('#alokasi').is(':checked'),
                implementasi: $('#implementasi').is(':checked'),
                promo_stock: $('#promo_stock').is(':checked'),
                promo_mekanisme: $('#promo_mekanisme').is(':checked'),
                promo_periode: $('#promo_periode').is(':checked'),
                promo_posm: $('#promo_posm').is(':checked'),
                promo_harga: $('#promo_harga').is(':checked'),
                company: $('#company').val(),
                group: $('#group').val(),
                examtype: $('#examtype').val(),
                method: $('#method').val(),
                grade: $('#grade').val(),
                fase: $('#fase').val(),
            },
        })
            .done(function (result, status, xhr) {
                // The actual download
                var blob = new Blob([result], {
                    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                });
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = filename;

                document.body.appendChild(link);

                link.click();
                document.body.removeChild(link);

                // Revert button to normal state
                exportButton.prop('disabled', false);
                exportButton.html('<i class="bx bxs-file-doc"></i> Export');
            })
            .fail(function (err) {
                // Revert button to normal state
                exportButton.prop('disabled', false);
                exportButton.html('<i class="bx bxs-file-doc"></i> Export');
            });
        return false;
    });

    $('#btn-logout').click(function (e) {
        e.preventDefault();
        let url = $(this).attr('href');

        Swal.fire({
            title: 'Are you sure you want to log out?',
            // text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#499fe4',
            cancelButtonColor: '#AEAEAE',
            confirmButtonText: 'Yes, Logout!',
            cancelButtonText: 'Cancel',
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = url;
            }
        });
    });

    $('.table').on('click', '.btn-hapus', function (e) {
        e.preventDefault();
        let url = $(this).attr('href');
        let title = $(this).data('title');
        let status = $(this).data('status');

        let textMessage = "The data to be deleted : " + title;
        let btnText = 'Yes, Delete!';

        if (status == false || status === 'n') {
            textMessage = "The data to be reactivated : " + title;
            btnText = 'Reactivated!';
        }

        Swal.fire({
            title: 'Are you sure?',
            text: textMessage,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#499fe4',
            cancelButtonColor: '#AEAEAE',
            confirmButtonText: btnText,
            cancelButtonText: 'Cancel',
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'DELETE',
                    url: url,
                    data: {
                        _token: $('input[name="_token"]').val(),
                    },
                })
                    .done(function (e) {
                        Swal.fire('Success!', e.pesan, 'success');
                        datatables.ajax.reload(null, false);
                    })
                    .fail(function () {
                        Swal.fire('Error!', e.pesan, 'error');
                    });
            }
        });
    });

});
